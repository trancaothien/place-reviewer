import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';
import 'package:flutter_place_reviewer/src/presentation/routers.dart';
import 'package:flutter_place_reviewer/src/presentation/splash/splash.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      systemNavigationBarColor: Colors.white,
      statusBarIconBrightness: Brightness.dark));
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: normalTheme(context),
      home: SplashScreen(),
      builder: EasyLoading.init(),
      onGenerateRoute: (RouteSettings settings) {
        return Routers.generateRoute(settings);
      },
    );
  }

  // void showNotification() async {
  //   User user = await AppShared().getAccount();
  //   FirebaseRealTime().checkNewMessage(user.uid, (chat) => {
  //
  //   });
  // }

}
