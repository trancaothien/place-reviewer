class ChatModel {
  String chatId;
  String sender;
  String receiver;
  String message;
  int createTime;
  int status;

  ChatModel(
      {this.sender, this.receiver, this.message, this.createTime, this.status});

  ChatModel.fromJson(String id, Map data) {
    chatId = id;
    sender = data['sender'];
    receiver = data['receiver'];
    message = data['message'];
    createTime = data['createTime'];
    status = data['status'];
  }

  Map<String, dynamic> toMap() {
    return {
      'sender': sender,
      'receiver': receiver,
      'message': message,
      'createTime': createTime,
      'status': status
    };
  }

  @override
  String toString() {
    return 'Chat{sender: $sender, receiver: $receiver, message: $message, createTime: $createTime, status: $status}';
  }
}
