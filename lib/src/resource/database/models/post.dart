

class Post {
  String postId;
  String postTime;
  String placeName;
  String countryName;
  String contentOfPost;
  String uriOfImage;
  String categoryOfPost;
  String uid;
  int view;

  Post(
      {this.postId, this.postTime,
      this.placeName,
      this.contentOfPost,
      this.countryName,
      this.uriOfImage,
      this.categoryOfPost,
      this.uid,
      this.view});

  Post.fromJson(String id, Map data) {
    postId = id;
    postTime = data['postTime'];
    placeName = data['placeName'];
    countryName = data['countryName'];
    contentOfPost = data['contentOfPost'];
    uriOfImage = data['uriOfImage'];
    categoryOfPost = data['category'];
    uid = data['uid'];
    view = data['view'];
  }

  Map<String, dynamic> toMap(){
    return {
      'postId': postId,
      'postTime': postTime,
      'placeName': placeName,
      'countryName': countryName,
      'contentOfPost': contentOfPost,
      'uriOfImage': uriOfImage,
      'category': categoryOfPost,
      'uid': uid,
      'view': view
    };
  }

  @override
  String toString() {
    return 'Post{postTime: $postTime, placeName: $placeName, countryName: $countryName, contentOfPost: $contentOfPost, uriOfImage: $uriOfImage, categoryOfPost: $categoryOfPost, uid: $uid, view: $view}';
  }
}
