class Profile {
  String email;
  String imageBG;
  String avatar;
  String name;

  Profile({this.email, this.avatar, this.imageBG, this.name});

  Profile.fromJson(Map data) {
    email = data['email'];
    imageBG = data['coverImage'];
    avatar = data['avatar'];
    name = data['name'];
  }

  @override
  String toString() {
    return 'Profile{email: $email, imageBG: $imageBG, avatar: $avatar, name: $name}';
  }
}
