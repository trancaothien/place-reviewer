class FriendRequest{
  String requestId;
  String sender;
  String receiver;
  int status;
  int createTime;

  FriendRequest({this.sender, this.receiver, this.status, this.createTime});

  FriendRequest.fromJson(String key, Map data) {
    requestId = key;
    sender = data['sender'];
    receiver = data['receiver'];
    status = data['status'];
    createTime = data['createTime'];
  }

  Map<String, dynamic> toMap(){
    return {
      'sender': sender,
      'receiver': receiver,
      'status': status,
      'createTime': createTime,
    };
  }

  @override
  String toString() {
    return 'FriendRequest{sender: $sender, receiver: $receiver, status: $status, createTime: $createTime}';
  }
}