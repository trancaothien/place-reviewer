import 'dart:io';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter_place_reviewer/src/resource/database/firebase/storage_firebase.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/chat_model.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/friend_request.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/models.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/profile.dart';
import 'package:flutter_place_reviewer/src/utils/utils.dart';

class FirebaseRealTime {
  static FirebaseRealTime database;

  static FirebaseRealTime getInstance() {
    if (database == null) {
      database = FirebaseRealTime();
      return database;
    } else
      return database;
  }

  //createUser when signUp is success.
  createUser(String name, String email, String uid) async {
    var user = {"name": name, "email": email};
    var ref = FirebaseDatabase.instance.reference().child('user');
    await ref.child(uid).set(user);
  }

  // addNewPost in firebase.
  addNewPost(File image, String place, String content, String country,
      int currentTime, String category, success(bool result)) async {
    User user = await AppShared().getAccount();
    String result = await StorageFirebase()
        .uploadImage(image, user.uid, currentTime.toString());
    Post post = Post(
        postId: "${user.uid}-$currentTime",
        postTime: currentTime.toString(),
        placeName: place,
        contentOfPost: content,
        countryName: country,
        uriOfImage: result,
        categoryOfPost: category,
        uid: user.uid,
        view: 1);

    if (result != null) {
      var ref = FirebaseDatabase.instance.reference().child('posts');
      await ref
          .child(post.postId)
          .set(post.toMap())
          .then((value) => success(true));
    }
    return success(false);
  }

  // get the posts popular... is 4, 5, or...
  selectPopularDestination(success(Post post)) {
    DatabaseReference dbRef =
        FirebaseDatabase.instance.reference().child('posts');
    dbRef.orderByChild('view').limitToLast(4).onChildAdded.forEach(
      (element) {
        Post post = Post.fromJson(element.snapshot.key, element.snapshot.value);
        success(post);
      },
    );
  }

  getHotel() {}

  //getDetailPost
  getDetailPost(String postId, success(Post post)) {
    DatabaseReference dbRef =
        FirebaseDatabase.instance.reference().child('posts');
    dbRef.orderByChild('postTime').onChildAdded.forEach(
      (element) {
        Post post = Post.fromJson(element.snapshot.key, element.snapshot.value);
        if (post.postId == postId) {
          print(element.snapshot.value);
          success(post);
          return;
        }
      },
    );
  }

  //getPostWithPageView
  getPostWithPageView(String postID, int pageLimit, success(Post posts)) {
    DatabaseReference dbRef =
        FirebaseDatabase.instance.reference().child('posts');
    int count = 0;
    if (postID != null) {
      dbRef
          .orderByKey()
          .limitToFirst(pageLimit)
          .startAt(postID)
          .onChildAdded
          .listen((element) {
        print(element.snapshot.key);
        Post post = Post.fromJson(element.snapshot.key, element.snapshot.value);
        if (count > 0) {
          count++;
          success(post);
        } else {
          count++;
        }
      });
    } else {
      dbRef.orderByKey().limitToFirst(pageLimit).onChildAdded.listen((element) {
        print(element.snapshot.key);
        Post post = Post.fromJson(element.snapshot.key, element.snapshot.value);
        success(post);
        count++;
      });
    }
  }

  getAllPopularDestination(String postID, int pageLimit, success(Post posts)) {
    DatabaseReference dbRef =
        FirebaseDatabase.instance.reference().child('posts');
    if (postID != null) {
      dbRef
          .startAt(postID)
          .limitToLast(pageLimit)
          .orderByChild("view")
          .onChildAdded
          .listen((event) {
        print(event.snapshot.key);
        Post post = Post.fromJson(event.snapshot.key, event.snapshot.value);
        success(post);
      });
    } else {
      dbRef
          .orderByChild("view")
          .limitToLast(pageLimit)
          .onChildAdded
          .listen((event) {
        print(event.snapshot.key);
        Post post = Post.fromJson(event.snapshot.key, event.snapshot.value);
        success(post);
      });
    }
  }

  //getPostWithGroup
  getPostWithCountryAndCategory(
      String country, String category, success(Post post)) {
    DatabaseReference dbRef =
        FirebaseDatabase.instance.reference().child('posts');
    dbRef.orderByChild('postTime').onChildAdded.forEach((element) {
      Post post = Post.fromJson(element.snapshot.key, element.snapshot.value);
      if (country == null && category == null) {
        success(post);
      } else if (country == null && category != null) {
        if (post.categoryOfPost == category) {
          success(post);
        }
      } else if (country != null && category == null) {
        if (post.countryName == country) {
          success(post);
        }
      } else {
        if (post.countryName == country && post.categoryOfPost == category) {
          print(element.snapshot.value);
          success(post);
        }
      }
    });
  }

  //get post with uid
  getPostsWithUid(String uid, success(Post post)) {
    DatabaseReference dbRef =
        FirebaseDatabase.instance.reference().child('posts');
    dbRef.onChildAdded.forEach((element) {
      Post post = Post.fromJson(element.snapshot.key, element.snapshot.value);
      if (post.uid == uid) {
        success(post);
      }
    });
  }

  //get profile with email
  getProfile(String email, success(Profile profile)) {
    DatabaseReference dbRef =
        FirebaseDatabase.instance.reference().child('user');
    dbRef.onChildAdded.forEach((element) {
      Profile profile = Profile.fromJson(element.snapshot.value);
      if (profile.email == email) {
        success(profile);
      }
    });
  }

  //update view when show detail post
  updateViewOfPost(String postId, int views) async {
    var ref = FirebaseDatabase.instance.reference().child('posts');
    await ref.child(postId).child('view').set(views);
  }

  // update coverImage
  Future<bool> updateCoverImage(File coverImage) async {
    User user = await AppShared().getAccount();
    String result =
        await StorageFirebase().uploadCoverImage(coverImage, user.uid);

    if (result != null) {
      await FirebaseDatabase.instance
          .reference()
          .child('user')
          .child(user.uid)
          .child('coverImage')
          .set(result);
      return true;
    } else {
      return false;
    }
  }

  Future<bool> updateAvatar(File avatar) async {
    User user = await AppShared().getAccount();
    String result = await StorageFirebase().uploadAvatar(avatar, user.uid);

    if (result != null) {
      await FirebaseDatabase.instance
          .reference()
          .child('user')
          .child(user.uid)
          .child('avatar')
          .set(result);

      return true;
    } else {
      return false;
    }
  }

  addBookmark(String postId, success(bool)) async {
    User user = await AppShared().getAccount();
    FirebaseDatabase.instance
        .reference()
        .child('bookmarks')
        .child(postId)
        .child(user.uid)
        .set(postId)
        .then((value) => success(true));
  }

//get bookmark by id bookmark
  getBookmark(success(Post post)) async {
    await _getPostIdFromBookmarks(
      (String postIDs) {
        if (postIDs != null) {
          FirebaseDatabase.instance
              .reference()
              .child('posts')
              .child(postIDs)
              .once()
              .then((value) {
            print(value.value);
            success(Post.fromJson(value.key, value.value));
          });
        }
      },
    );
  }

//get only id in bookmark
  _getPostIdFromBookmarks(success(String postID)) async {
    User user = await AppShared().getAccount();

    DatabaseReference dbRef =
        FirebaseDatabase.instance.reference().child('bookmarks');
    await dbRef.onChildAdded.forEach(
      (Event element) {
        String value = element.snapshot.value[user.uid];
        print('abd $value');
        success(value);
      },
    );
  }

  checkBookmark(String postId, success(bool isHaveBookmark)) async {
    User user = await AppShared().getAccount();
    FirebaseDatabase.instance
        .reference()
        .child('bookmarks')
        .child(postId)
        .child(user.uid)
        .onValue
        .forEach((element) {
      if (element.snapshot.value != null) {
        success(true);
      }
    });
  }

  // remove bookmark
  removeBookmark(String postId, success()) async {
    User user = await AppShared().getAccount();
    FirebaseDatabase.instance
        .reference()
        .child('bookmarks')
        .child(postId)
        .child(user.uid)
        .remove()
        .then((value) => success());
  }

  searchWithPlaceName(String value, success(Post post)) {
    DatabaseReference dbRef =
        FirebaseDatabase.instance.reference().child('posts');
    dbRef.onChildAdded.forEach((element) {
      Post post = Post.fromJson(element.snapshot.key, element.snapshot.value);
      if (post.placeName.contains(value) || post.countryName.contains(value)) {
        success(post);
      }
    });
  }

  // get uid from email
  getUid(String email, success(String uid)) {
    DatabaseReference dbRef =
        FirebaseDatabase.instance.reference().child('user');
    dbRef.onChildAdded.forEach((element) {
      Profile profile = Profile.fromJson(element.snapshot.value);
      if (profile.email == email) {
        success(element.snapshot.key);
        return;
      }
    });
  }

  //get Profile
  getProfileWithUid(String uid, onResult(Profile profile)) {
    DatabaseReference dbRef =
        FirebaseDatabase.instance.reference().child('user');
    dbRef.onChildAdded.forEach((element) {
      if (element.snapshot.key == uid) {
        Profile profile = Profile.fromJson(element.snapshot.value);
        onResult(profile);
        return;
      }
    });
  }

  //add friend
  addRequest(FriendRequest friendRequest, success(bool result)) {
    Map<String, dynamic> value = friendRequest.toMap();
    FirebaseDatabase.instance
        .reference()
        .child('requestFriend')
        // id of request: senderid-receiverid
        .child('${friendRequest.sender}-${friendRequest.receiver}')
        .set(value)
        .then((value) => success(true));
  }

  // get all request
  getAllRequestFriend(String receiver, success(FriendRequest friendRequest)) {
    print(receiver);
    DatabaseReference dbRef =
        FirebaseDatabase.instance.reference().child('requestFriend');
    dbRef.onChildAdded.forEach((element) {
      FriendRequest request =
          FriendRequest.fromJson(element.snapshot.key, element.snapshot.value);
      print(request.toString());
      if (request.receiver == receiver && request.status == 0) {
        success(request);
      }
    });
  }

  // accept request when click button accept
  acceptRequest(
      String uid, String sender, String requestId, success(bool result)) {
    FirebaseDatabase.instance
        .reference()
        .child('requestFriend')
        .child(requestId)
        .child('status')
        // 1: accept request
        .set(1)
        .then((value) {
      _addFriend(uid, sender, (result) {
        success(result);
      });
    });
  }

  // add new friend
  _addFriend(String uid, String sender, success(bool result)) {
    FirebaseDatabase.instance
        .reference()
        .child('friend')
        .child(uid)
        .push()
        .child('uid')
        .set(sender)
        .then((value) {
      FirebaseDatabase.instance
          .reference()
          .child('friend')
          .child(sender)
          .push()
          .child('uid')
          .set(uid)
          .then((value) {
        success(true);
      });
    });
  }

  //get all friend from uid
  getAllFriend(String uid, success(String uid)) {
    DatabaseReference dbRef =
        FirebaseDatabase.instance.reference().child('friend').child(uid);
    dbRef.onChildAdded.forEach((element) {
      String uid = element.snapshot.value['uid'];
      success(uid);
    });
  }

  // send message
  sendMessage(ChatModel chat, onResult(bool result)) {
    Map<String, dynamic> value = chat.toMap();
    FirebaseDatabase.instance
        .reference()
        .child('chats')
        .push()
        .set(value)
        .then((value) {
      onResult(true);
    });
  }

  // get All message with uid
  getAllMessageWithUid(
      String receiver, String sender, onResult(ChatModel chat)) {
    DatabaseReference dbRef =
        FirebaseDatabase.instance.reference().child('chats');
    dbRef.onChildAdded.forEach((element) {
      ChatModel chat =
          ChatModel.fromJson(element.snapshot.key, element.snapshot.value);
      if (chat.sender == receiver && chat.receiver == sender) {
        onResult(chat);
      } else if (chat.sender == sender && chat.receiver == receiver) {
        onResult(chat);
      }
    });
  }

  //return Chat if have new Message
  checkNewMessage(String uid, onResult(ChatModel chat)) async {
    DatabaseReference dbRef =
        FirebaseDatabase.instance.reference().child('chats');
    await dbRef.onChildAdded.forEach((element) {
      ChatModel chat =
          ChatModel.fromJson(element.snapshot.key, element.snapshot.value);
      if (chat.receiver == uid && chat.status == 0) {
        onResult(chat);
      }
    });
  }

  // update status = 1 when open chat screen
  updateMessage(ChatModel chat) {
    FirebaseDatabase.instance
        .reference()
        .child('chats')
        .child(chat.chatId)
        .child('status')
        .set(1);
  }

  void checkHaveFriend(String friendId, String uid, onResult(bool result)) {
    DatabaseReference dbRef =
        FirebaseDatabase.instance.reference().child('friend').child(uid);
    bool isHaveFriendBefore = true;
    dbRef.onChildAdded.forEach((element) {
      String id = element.snapshot.value['uid'];
      if (id == friendId) {
        onResult(false);
        return;
      } else {
        isHaveFriendBefore = false;
      }
    });
    if (isHaveFriendBefore) {
      onResult(true);
    }
  }

  // get count of friend
  getCountFriend(String uid, success(int result)) {
    DatabaseReference dbRef =
        FirebaseDatabase.instance.reference().child('friend').child(uid);
    dbRef.once().then((dataSnapshot) {
      Map<dynamic, dynamic> values = dataSnapshot.value;
      success(values.length);
    });
  }

  //get count of friendRequest
  getCountFriendRequest(String uid, success(int result)) {
    int count = 0;
    DatabaseReference dbRef =
        FirebaseDatabase.instance.reference().child('requestFriend');
    dbRef.onChildAdded.forEach((element) {
      FriendRequest friendRequest =
          FriendRequest.fromJson(element.snapshot.key, element.snapshot.value);
      if (friendRequest.receiver == uid) {
        if (friendRequest.status == 0) {
          count++;
          success(count);
        }
      }
    });
  }
}
