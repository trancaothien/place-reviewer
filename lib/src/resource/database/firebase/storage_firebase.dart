import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';

class StorageFirebase {
  Future<String> uploadImage(File image, String uid, String currentTime) async {
    final Reference storage =
        FirebaseStorage.instance.ref().child('imagePosts/$uid/$currentTime');
    await storage.putFile(image);
    return storage.getDownloadURL();
  }

  Future<String> uploadCoverImage(File image, String uid) async {
    final Reference storage =
        FirebaseStorage.instance.ref().child('profiles/$uid/coverImage');
    await storage.putFile(image);
    return storage.getDownloadURL();
  }

  Future<String> uploadAvatar(File image, String uid) async {
    final Reference storage =
        FirebaseStorage.instance.ref().child('profiles/$uid/avatar');
    await storage.putFile(image);
    return storage.getDownloadURL();
  }
}
