import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_place_reviewer/src/resource/database/firebase/firebase_realtime.dart';
import 'package:flutter_place_reviewer/src/utils/utils.dart';

class FirebaseAuthencation {
  static FirebaseAuthencation auth = null;

  static FirebaseAuthencation getInstance() {
    if (auth == null) {
      auth = FirebaseAuthencation();
      return auth;
    } else
      return auth;
  }

  FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  Future<String> signUp(String pass, String email, name) async {
    try {
      await _firebaseAuth
          .createUserWithEmailAndPassword(email: email, password: pass)
          .then((value) =>
              FirebaseRealTime().createUser(name, email, value.user.uid));
      return 'success';
    } on FirebaseAuthException catch (e) {
      return e.code;
    }
  }

  Future<String> signIn(String email, String pass) async {
    try {
      await _firebaseAuth
          .signInWithEmailAndPassword(email: email, password: pass)
          .then((value) {
        AppShared().saveAccount(email, value.user.uid, pass);
      });
      return 'success';
    } on FirebaseAuthException catch (e) {
      return e.code;
    }
  }

  String resetPassword(String email) {
    _firebaseAuth.sendPasswordResetEmail(email: email);
    return 'success';
  }
}
