import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_place_reviewer/src/utils/utils.dart';

class FirebaseMessage {
  final FirebaseMessaging firebaseMessaging;

  FirebaseMessage(this.firebaseMessaging);

  initFirebaseMessaging() async {
    // if (Platform.isIOS) {
    //   print('=====> platform is IOS');
    //   await firebaseMessaging.requestPermission(
    //       sound: true, badge: true, alert: true, provisional: true);
    //   firebaseMessaging.onIosSettingsRegistered
    //       .listen((IosNotificationSettings settings) {
    //     print("Settings registered: $settings");
    //   });
    // }
    //   firebaseMessaging.getToken().then((String token) {
    //     if (token != null) AppShared.setFirebaseToken(token);
    //   });
    //   firebaseMessaging.configure(
    //       onMessage: (Map<String, dynamic> message) async {
    //         print("onMessage $message ");
    //         handler(message, onlyShow: true);
    //       },
    //       onLaunch: (Map<String, dynamic> message) async {
    //         print("onLaunch $message ");
    //         handler(message);
    //       },
    //       onResume: (Map<String, dynamic> message) async {
    //         print("onResume $message ");
    //         handler(message);
    //       },
    //       onBackgroundMessage:
    //           Platform.isIOS ? null : myBackgroundMessageHandler);
    // }

    // handler(Map<String, dynamic> message, {bool onlyShow = false}) {
    //   FirebaseNotification firebaseNotification =
    //       FirebaseNotification.fromJson(message);
    //   if (onlyShow) {
    //     LocalNotification.showNotification(
    //         firebaseNotification.notification.title,
    //         firebaseNotification.notification.body,
    //         firebaseNotification.data.toString());
    //   } else {
    //     selectNotificationSubject.add(firebaseNotification.data.toString());
    //   }
  }
}
