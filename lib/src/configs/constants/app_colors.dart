import 'package:flutter/material.dart';

class AppColors{
  AppColors._();
  static final Color orangeColor = Color.fromRGBO(243, 128, 0, 1);
  static final Color blueColor = Color.fromRGBO(49, 45, 164, 1);
  static final Color greyColor = Color.fromRGBO(226, 226, 226, 1);
}