import 'package:flutter/material.dart';

class AppStyles {
  AppStyles._();
  static const String FONT_BOLD = "PoppinsBold";
  static const String FONT_MEDIUM = "PoppinsMedium";

  static const FONT_SIZE_VERY_SMALL = 10.0;
  static const FONT_SIZE_SMALL = 14.0;
  static const FONT_SIZE_MEDIUM = 20.0;
  static const FONT_SIZE_LARGE = 30.0;

  static const TEXT_HEIGHT = 1.2;

  static const DEFAULT_SMALL =
      TextStyle(fontSize: FONT_SIZE_SMALL, height: TEXT_HEIGHT);
  static const DEFAULT_MEDIUM =
      TextStyle(fontSize: FONT_SIZE_MEDIUM, height: TEXT_HEIGHT);
  static const DEFAULT_LARGE =
      TextStyle(fontSize: FONT_SIZE_LARGE, height: TEXT_HEIGHT);

  static final DEFAULT_SMALL_BOLD =
      DEFAULT_SMALL.copyWith(fontWeight: FontWeight.bold);
  static final DEFAULT_MEDIUM_BOLD =
      DEFAULT_MEDIUM.copyWith(fontWeight: FontWeight.bold);
  static final DEFAULT_LARGE_BOLD =
      DEFAULT_LARGE.copyWith(fontWeight: FontWeight.bold);
}
