class AppDefaults{
  AppDefaults._();
  static const String homeTitle = 'Home';
  static const String categoryTitle = 'Category';
  static const String bookMarkTitle = 'Bookmark';
  static const String addPostTitle = 'Add Post';
  static const String forgotPass = 'Forgot password';
  static const String createAccount = 'Don\'t have account? Create account';


  //result of firebaseAuth
  static const String invalidEmail = 'invalid-email';
  static const String emailAlreadyInUse = 'email-already-in-use';
  static const String operationNotAllowed = 'operation-not-allowed';
  static const String weakPassword = 'weak-password';
  static const String userDisabled = 'user-disabled';
  static const String userNotFound = 'user-not-found';
  static const String wrongPassword = 'wrong-password';

  static const String notConfirmPass = 'not-confirm-pass';

  //countryName
  static const String keyUserName = 'EMAIL_OF_USER';
  static const String keyUid = 'UID';
  static const String keyPass = 'PASS';

  //places api
  static const String API_KEY = 'AIzaSyCcuTU7kD2JsGiiAeo-r2OkXwvRhDcAibg';

  //isLogout
  static const String keyLogOut = 'IS_LOGOUT';
}