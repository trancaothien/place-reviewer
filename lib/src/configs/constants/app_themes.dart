import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';
import 'app_styles.dart';

normalTheme(BuildContext context) {
  return ThemeData(
    backgroundColor: Colors.white,
    fontFamily: AppStyles.FONT_MEDIUM,
    disabledColor: Color(0xFFEDEDED),
    cardColor: Colors.white,
    canvasColor: Colors.white,
    brightness: Brightness.light,
    buttonTheme: Theme.of(context).buttonTheme.copyWith(
        colorScheme: ColorScheme.light(),
        buttonColor: Color(0xFFF38000),
        splashColor: Colors.white),
    appBarTheme: AppBarTheme(
      color: Colors.white,
      elevation: 0.0,
    ),
  );
}
