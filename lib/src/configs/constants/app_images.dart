class AppImages {
  AppImages._();

  static final String iconHome = 'assets/images/home.png';
  static final String iconGroup = 'assets/images/Group.png';
  static final String iconBookmark = 'assets/images/bookmark.png';
  static final String iconProfile = 'assets/images/usericon.png';
  static final String iconUser = 'assets/images/user.png';
  static final String iconPass = 'assets/images/key.png';
  static final String iconEmail = 'assets/images/email.png';
  static final String iconCamera = 'assets/images/photo-camera.png';
  static final String iconGallery = 'assets/images/gallery.png';
  static final String backgroundSplash1 = 'assets/images/Group_260.png';
  static final String backgroundSplash2 = 'assets/images/Group_261.png';
  static final String background1 = 'assets/images/Group_273.png';
  static final String background2 = 'assets/images/Group_274.png';
  static final String iconHamburgerMenu = 'assets/images/list.png';
  static final String iconSearch = 'assets/images/magnifying-glass.png';
  static final String iconArrowRight = 'assets/images/left-arrow.png';
  static final String iconBackArrow = 'assets/images/back_arrow.png';
  static final String iconAddBookmark = 'assets/images/add_bookmark.png';
  static final String iconHaveBookmark = 'assets/images/isbookmark.png';
  static final String defaultAvatar = 'assets/images/iconProfile.png';
  static final String iconViews = 'assets/images/view.png';
  static final String iconAddNewBookmark = 'assets/images/bookmark_detail.png';
  static final String iconSendMessage = 'assets/images/ic_send.png';
  static final String backgroundSplash3 = 'assets/images/background.png';
}
