import 'package:flutter_place_reviewer/src/presentation/base/base.dart';
import 'package:flutter_place_reviewer/src/resource/database/firebase/firebase_realtime.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/post.dart';

class HomeViewModel extends BaseViewModel {
  List<Post> posts = [];
  FirebaseRealTime database = FirebaseRealTime.getInstance();

  void getPopularDestination() {
    database.selectPopularDestination((post) {
      posts.add(post);
      notifyListeners();
    });
  }
}
