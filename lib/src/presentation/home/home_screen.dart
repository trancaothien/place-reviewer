import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';
import 'package:flutter_place_reviewer/src/presentation/home/home.dart';
import 'package:flutter_place_reviewer/src/presentation/presentation.dart';
import 'package:flutter_place_reviewer/src/presentation/widgets/widget_item_popular.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<String> temp;
  GlobalKey<ScaffoldState> _scaffoldKey;
  double _appBarHeight = AppBar().preferredSize.height;
  @override
  Widget build(BuildContext context) {
    _scaffoldKey = new GlobalKey<ScaffoldState>();
    return BaseWidget<HomeViewModel>(
      viewModel: HomeViewModel(),
      onViewModelReady: (viewModel) {
        viewModel.getPopularDestination();
      },
      builder: (context, viewModel, child) {
        return _bodyBuilder(context, viewModel);
      },
    );
  }

  Widget _bodyBuilder(BuildContext context, HomeViewModel viewModel) {
    temp = _initTemp();
    return Scaffold(
      key: _scaffoldKey,
      drawer: Drawer(
        child: NavigationScreen(),
      ),
      body: _bodyHome(context, viewModel),
    );
  }

  Widget _appBar() {
    return Container(
      height: _appBarHeight + 20,
      padding: EdgeInsets.only(top: _appBarHeight / 2),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: IconButton(
              icon: Image.asset(
                AppImages.iconHamburgerMenu,
                fit: BoxFit.cover,
              ),
              onPressed: () {
                _scaffoldKey.currentState.openDrawer();
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 20),
            child: Center(
              child: Text(
                AppDefaults.homeTitle,
                style: TextStyle(
                    fontSize: AppStyles.FONT_SIZE_LARGE,
                    color: AppColors.blueColor,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _cardCountry(String nameCountry) {
    return Container(
      margin: EdgeInsets.only(left: 5),
      width: 100,
      height: 50,
      child: Stack(
        children: [
          Image.asset('assets/images/usa.png'),
          Center(
            child: Text(
              nameCountry,
              style: TextStyle(
                  fontSize: AppStyles.FONT_SIZE_SMALL, color: Colors.white),
            ),
          )
        ],
      ),
    );
  }

  Widget _exploreCard(String name) {
    return Container(
      padding: EdgeInsets.only(left: 10, bottom: 10),
      width: 125,
      height: 200,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(20.0),
        ),
      ),
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: Image.asset(
              'assets/images/$name.jpg',
              fit: BoxFit.cover,
              width: 125,
              height: 200,
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Text(
              name,
              style: TextStyle(color: Colors.white),
            ),
          )
        ],
      ),
    );
  }

  List<String> _initTemp() {
    List<String> strings = <String>[];
    strings.add('Canada');
    strings.add('USA');
    strings.add('Việt Nam');
    strings.add('Thailand');
    strings.add('Korean');
    return strings;
  }

  Widget _listPopular(HomeViewModel viewModel, BuildContext context) {
    // int _length = viewModel.posts.length();
    return viewModel.posts.isNotEmpty
        ? CarouselSlider(
            items: viewModel.posts.map(
              (post) {
                return WidgetItemPopular(
                  post: post,
                  onPress: () => Navigator.pushNamed(
                      context, Routers.detailPost,
                      arguments: post.postId),
                );
              },
            ).toList(),
            options: CarouselOptions(
              height: 400.0,
              enlargeCenterPage: true,
              aspectRatio: 1.0,
              enlargeStrategy: CenterPageEnlargeStrategy.height,
            ),
          )
        : Center(
            child: CircularProgressIndicator(),
          );
  }

  Widget _showPostBar() {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, Routers.showAllPost,
            arguments: 'Show Post');
      },
      child: Container(
        // Show post
        height: 40.0,
        decoration: BoxDecoration(
          border: Border(
            top: BorderSide(width: 1.0, color: AppColors.greyColor),
            bottom: BorderSide(width: 1.0, color: AppColors.greyColor),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  'Show post',
                  style: TextStyle(fontSize: AppStyles.FONT_SIZE_SMALL),
                ),
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Image.asset(
                    AppImages.iconArrowRight,
                    scale: 2,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _bodyHome(BuildContext context, HomeViewModel viewModel) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(AppImages.backgroundSplash3),
          fit: BoxFit.fill,
        ),
      ),
      child: SingleChildScrollView(
        child: Column(
          children: [
            _appBar(),
            _showPostBar(),
            Container(
              // list Country
              margin: EdgeInsets.only(top: 10, bottom: 10),
              height: 50,
              width: MediaQuery.of(context).size.width,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: temp.length,
                itemBuilder: (context, index) {
                  return _cardCountry(temp[index]);
                },
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.pushNamed(context, Routers.showAllPost,
                    arguments: 'Popular Destination');
              },
              child: Container(
                // popular destination
                height: 40.0,
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Popular Destination',
                        style: TextStyle(fontSize: AppStyles.FONT_SIZE_SMALL),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        'View All',
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: AppStyles.FONT_SIZE_SMALL),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              // card popular
              constraints: new BoxConstraints(
                minHeight: 400.0,
              ),
              width: MediaQuery.of(context).size.width,
              // padding: EdgeInsets.only(left: 10, right: 10),
              child: _listPopular(viewModel, context),
            ),
            Container(
              // padding: EdgeInsets.only(top: 10, right: 10),
              height: 200,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  _exploreCard('Nature'),
                  _exploreCard('City'),
                  _exploreCard('River'),
                  _exploreCard('Mountain'),
                  _exploreCard('Lake'),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
