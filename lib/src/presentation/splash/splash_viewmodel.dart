import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_place_reviewer/src/presentation/base/base.dart';
import 'package:flutter_place_reviewer/src/resource/database/firebase/firebase_authencation.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/user.dart';
import 'package:flutter_place_reviewer/src/utils/utils.dart';

class SplashViewModel extends BaseViewModel {
  init() async {
    await Firebase.initializeApp();
  }

  Future<bool> checkUser() async {
    FirebaseAuthencation authencation = FirebaseAuthencation();

    User user = await AppShared().getAccount();
    if (user.uid == null || user.email == null || user.pass == null) {
      return false;
    } else {
      String result = await authencation.signIn(user.email, user.pass);
      if (result == 'success') {
        return true;
      }
      return false;
    }
  }
}
