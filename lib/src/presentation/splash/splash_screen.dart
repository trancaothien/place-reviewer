import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';
import 'package:flutter_place_reviewer/src/presentation/presentation.dart';
import 'package:flutter_place_reviewer/src/presentation/widgets/widget_button_blur.dart';
import 'package:flutter_place_reviewer/src/utils/utils.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<SplashViewModel>(
      viewModel: SplashViewModel(),
      onViewModelReady: (viewModel) async {
        await viewModel.init();
      },
      builder: (context, viewModel, child) {
        return _buildBody(context, viewModel);
      },
    );
  }

  Widget _buildBody(BuildContext context, SplashViewModel viewModel) {
    return Scaffold(
      body: Container(
        child: Stack(
          children: [
            Align(
              child: Image.asset(
                AppImages.backgroundSplash3,
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                fit: BoxFit.cover,
              ),
            ),
            _infoApp(context, viewModel),
            Padding(
              padding: const EdgeInsets.only(bottom: 20),
              child: _menu(context, viewModel),
            ),
          ],
        ),
      ),
    );
  }

  Widget _infoApp(BuildContext context, SplashViewModel viewModel) {
    return Align(
      alignment: Alignment.topLeft,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(top: 50, left: 30),
            child: Text(
              'Explore',
              style: TextStyle(
                  fontSize: AppStyles.FONT_SIZE_LARGE,
                  fontWeight: FontWeight.bold,
                  color: AppColors.blueColor,
                  height: 1.0),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10, left: 30),
            width: MediaQuery.of(context).size.width / 2 * 3,
            child: Text(
              'new amazing countries',
              style: AppStyles.DEFAULT_SMALL,
              textAlign: TextAlign.left,
            ),
          ),
        ],
      ),
    );
  }

  Widget _menu(BuildContext context, SplashViewModel viewModel) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 70,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            WidgetButtonBlur(
              name: 'Log In',
              onPress: () async {
                bool result = await viewModel.checkUser();
                bool isLogOut = await AppShared().isLogOut();
                if (result && !isLogOut) {
                  // Navigator.pushNamed(context, Routers.bottomNavigation);
                  Navigator.popAndPushNamed(context, Routers.bottomNavigation);
                } else {
                  // Navigator.pushNamed(context, Routers.loginScreen);
                  Navigator.popAndPushNamed(context, Routers.loginScreen);
                }
              },
            ),
            WidgetButtonBlur(
              name: 'Sign Up',
              onPress: () => Navigator.pushNamed(context, Routers.signUp),
            ),
          ],
        ),
      ),
    );
  }
}
