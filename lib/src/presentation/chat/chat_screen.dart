import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';
import 'package:flutter_place_reviewer/src/presentation/base/base.dart';
import 'package:flutter_place_reviewer/src/presentation/chat/chat.dart';
import 'package:flutter_place_reviewer/src/presentation/widgets/widget_item_leflt_chat.dart';
import 'package:flutter_place_reviewer/src/presentation/widgets/widget_item_right_chat.dart';
import 'package:flutter_place_reviewer/src/resource/resource.dart';

class ChatScreen extends StatefulWidget {
  String email;

  ChatScreen({this.email});

  @override
  _ChatScreenState createState() => _ChatScreenState(email: email);
}

class _ChatScreenState extends State<ChatScreen> {
  String email;

  _ChatScreenState({this.email});

  double heightInputMessage = AppBar().preferredSize.height;
  Profile profile;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<ChatViewModel>(
      viewModel: ChatViewModel(),
      onViewModelReady: (viewModel) async {
        viewModel.init(email).then((value) => profile = value);
        await viewModel.getAllMessage(email);
      },
      builder: (context, viewModel, child) {
        return _buildBody(context, viewModel);
      },
    );
  }

  Widget _buildBody(BuildContext context, ChatViewModel viewModel) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(AppImages.backgroundSplash3),
              fit: BoxFit.fill,
            ),
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          resizeToAvoidBottomInset: true,
          appBar: _appBar(context, viewModel),
          body: _bodyChat(context, viewModel),
        )
      ],
    );
  }

  Widget _bodyChat(BuildContext context, ChatViewModel viewModel) {
    return Stack(
      children: [
        Container(
          height: MediaQuery.of(context).size.height,
        ),
        Align(
          alignment: Alignment.topCenter,
          child: _listMessage(context, viewModel),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: _inputMessage(context, viewModel),
        ),
      ],
    );
  }

  Widget _listMessage(BuildContext context, ChatViewModel viewModel) {
    return Container(
        height: MediaQuery.of(context).size.height - heightInputMessage,
        margin: EdgeInsets.only(bottom: heightInputMessage),
        child: ListView.builder(
            itemCount: viewModel.chats.length,
            itemBuilder: (context, index) {
              if (viewModel.chats[index].sender == viewModel.user.uid) {
                return Align(
                  alignment: Alignment.centerRight,
                  child: WidgetItemRightChat(chat: viewModel.chats[index]),
                );
              } else {
                return Align(
                  alignment: Alignment.centerLeft,
                  child: WidgetItemLeftChat(
                    chat: viewModel.chats[index],
                  ),
                );
              }
            }));
  }

  Widget _inputMessage(BuildContext context, ChatViewModel viewModel) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(10),
      constraints: new BoxConstraints(
        minHeight: 50.0,
      ),
      child: Row(
        children: [
          Expanded(
            child: Container(
              margin: EdgeInsets.only(right: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(15)),
                color: Colors.grey.withOpacity(0.8),
              ),
              child: TextFormField(
                maxLines: null,
                controller: viewModel.edtInputMessage,
                decoration: new InputDecoration(
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    contentPadding:
                        EdgeInsets.only(left: 5, bottom: 5, top: 5, right: 5),
                    hintText: "Type message ... "),
              ),
            ),
            flex: 9,
          ),
          Expanded(
            child: Container(
              height: 50.0,
              padding: EdgeInsets.all(5.0),
              child: InkWell(
                child: Image.asset(
                  AppImages.iconSendMessage,
                  color: Colors.white,
                ),
                onTap: () {
                  viewModel.sendMessage(
                    email,
                    (result) {
                      if (result) {
                        viewModel.edtInputMessage.clear();
                      }
                    },
                  );
                },
              ),
            ),
            flex: 1,
          )
        ],
      ),
    );
  }

  Widget _appBar(BuildContext context, ChatViewModel viewModel) {
    return AppBar(
      backgroundColor: Colors.transparent,
      leading: IconButton(
        icon: Image.asset(
          AppImages.iconBackArrow,
          scale: 2,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.only(right: 20),
          child: Center(
            child: Text(
              viewModel.profileOfFriend.name,
              style: TextStyle(
                  fontSize: AppStyles.FONT_SIZE_LARGE,
                  color: AppColors.blueColor),
            ),
          ),
        )
      ],
      automaticallyImplyLeading: false,
    );
  }
}
