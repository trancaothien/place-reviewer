import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/resource/database/firebase/firebase_realtime.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/chat_model.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/profile.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/user.dart';
import 'package:flutter_place_reviewer/src/utils/utils.dart';
import '../presentation.dart';

class ChatViewModel extends BaseViewModel {
  FirebaseRealTime database = FirebaseRealTime.getInstance();
  Profile profileOfFriend;
  List<ChatModel> chats = [];
  User user;
  TextEditingController edtInputMessage = TextEditingController();

  init(String email) async {
    user = await AppShared().getAccount();
    notifyListeners();
    database.getProfile(email, (profile) {
      profileOfFriend = profile;
      notifyListeners();
    });
  }

  //send message with default status = 0 is not read before
  sendMessage(String email, onResult(bool success)) async {
    int currentTime = DateTime.now().millisecondsSinceEpoch;
    User user = await AppShared().getAccount();
    database.getUid(email, (uid) {
      ChatModel chat = ChatModel(
          createTime: currentTime,
          receiver: uid,
          sender: user.uid,
          message: edtInputMessage.text,
          status: 0);
      database.sendMessage(chat, (result) {
        onResult(result);
      });
    });
  }

//get all message and update check message when have new message
  getAllMessage(String email) async {
    User user = await AppShared().getAccount();
    database.getUid(email, (uid) {
      database.getAllMessageWithUid(user.uid, uid, (chat) {
        chats.add(chat);
        if (chat.sender == uid && chat.receiver == user.uid) {
          database.updateMessage(chat);
        }
        notifyListeners();
      });
    });
  }
}
