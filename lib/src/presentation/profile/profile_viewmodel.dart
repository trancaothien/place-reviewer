import 'dart:io';

import 'package:flutter_place_reviewer/src/presentation/presentation.dart';
import 'package:flutter_place_reviewer/src/resource/database/firebase/firebase_realtime.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/models.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/profile.dart';
import 'package:flutter_place_reviewer/src/utils/utils.dart';

class ProfileViewModel extends BaseViewModel {
  List<Post> posts = [];
  Profile profileOfUser = Profile();
  FirebaseRealTime database = FirebaseRealTime.getInstance();
  int countFriend = 0;
  int countRequest = 0;

  getPostsWithUid() async {
    User user = await AppShared().getAccount();
    database.getPostsWithUid(user.uid, (post) {
      posts.add(post);
      notifyListeners();
    });
  }

  getProfileWithUid() async {
    User user = await AppShared().getAccount();
    database.getProfile(user.email, (profile) {
      profileOfUser = profile;
      notifyListeners();
    });
  }

  updateCoverImage(File coverImage) async {
    bool result = await database.updateCoverImage(coverImage);
    if (result) getProfileWithUid();
  }

  updateAva(File avatar) async {
    bool result = await database.updateAvatar(avatar);
    if (result) {
      getProfileWithUid();
    } else {
      print("flase");
    }
  }

  getCountFriend() async {
    User user = await AppShared().getAccount();
    database.getCountFriend(user.uid, (result) {
      countFriend = result;
      notifyListeners();
    });
  }

  getCountFriendRequest() async {
    User user = await AppShared().getAccount();
    database.getCountFriendRequest(user.uid, (result) {
      countRequest = result;
      notifyListeners();
    });
  }
}
