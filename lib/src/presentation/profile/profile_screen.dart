import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';
import 'package:flutter_place_reviewer/src/presentation/presentation.dart';
import 'package:flutter_place_reviewer/src/presentation/profile/profile.dart';
import 'package:flutter_place_reviewer/src/presentation/widgets/widget_avatar.dart';
import 'package:flutter_place_reviewer/src/presentation/widgets/widget_request.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final picker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ProfileViewModel>(
      viewModel: ProfileViewModel(),
      onViewModelReady: (viewModel) async {
        await viewModel.getPostsWithUid();
        await viewModel.getProfileWithUid();
        await viewModel.getCountFriend();
        await viewModel.getCountFriendRequest();
      },
      builder: (context, viewModel, child) {
        return _buildBody(context, viewModel);
      },
    );
  }

  Widget _buildBody(BuildContext context, ProfileViewModel viewModel) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(AppImages.backgroundSplash3),
            fit: BoxFit.fill,
          ),
        ),
        child: Column(
          children: [
            Expanded(
              child: _profileBanner(viewModel),
              flex: 4,
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(top: 5),
                child: Column(
                  children: [
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            viewModel.profileOfUser.name != null
                                ? viewModel.profileOfUser.name
                                : '',
                            style: AppStyles.DEFAULT_MEDIUM,
                          ),
                          Text(
                            viewModel.profileOfUser.email != null
                                ? viewModel.profileOfUser.email
                                : '',
                            style: AppStyles.DEFAULT_SMALL,
                          ),
                        ],
                      ),
                      flex: 2,
                    ),
                    Expanded(
                      child: _infoFollow(viewModel),
                      flex: 3,
                    ),
                    Expanded(
                      child: _bodyProfile(viewModel),
                      flex: 15,
                    )
                  ],
                ),
              ),
              flex: 8,
            )
          ],
        ),
      ),
    );
  }

  Widget _bodyProfile(ProfileViewModel viewModel) {
    return Container(
      margin: EdgeInsets.only(right: 5, left: 5),
      child: Column(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Your post',
                  style: AppStyles.DEFAULT_SMALL,
                ),
              ),
            ),
            flex: 1,
          ),
          Expanded(
            child: Container(
              child: StaggeredGridView.countBuilder(
                crossAxisCount: 4,
                itemCount: viewModel.posts.length,
                itemBuilder: (BuildContext context, int index) =>
                    _cardPost(viewModel.posts[index].uriOfImage),
                staggeredTileBuilder: (int index) =>
                    new StaggeredTile.count(2, index.isEven ? 1 : 5),
              ),
            ),
            flex: 8,
          ),
        ],
      ),
    );
  }

  Widget _cardPost(String img) {
    return Container(
      margin: EdgeInsets.only(left: 5, right: 5, bottom: 5, top: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(20)),
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.black45, blurRadius: 5.0, offset: Offset(0.0, 0.75))
        ],
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15.0),
        child: CachedNetworkImage(
          placeholder: (context, url) => Container(
            color: Colors.white,
            child: Center(
              child: CircularProgressIndicator(
                strokeWidth: 3.0,
              ),
            ),
          ),
          imageUrl: img,
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget _infoFollow(ProfileViewModel viewModel) {
    return Container(
      margin: EdgeInsets.only(top: 10, bottom: 10),
      height: 50,
      child: Row(
        children: [
          Expanded(
            child: WidgetRequest(
              name: 'Friends',
              count: viewModel.countFriend,
              onPress: () {
                Navigator.pushNamed(context, Routers.friend);
              },
            ),
          ),
          Expanded(
            child: WidgetRequest(
              name: 'Friend requests',
              count: viewModel.countRequest,
              onPress: () {
                Navigator.pushNamed(context, Routers.friendRequest);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _profileBanner(ProfileViewModel viewModel) {
    return Stack(
      children: [
        Container(
          margin: EdgeInsets.only(bottom: 20),
          child: viewModel.profileOfUser.imageBG != null
              ? CachedNetworkImage(
                  placeholder: (context, url) => Container(
                    color: Colors.white,
                    child: Center(
                      child: CircularProgressIndicator(
                        strokeWidth: 3.0,
                      ),
                    ),
                  ),
                  imageUrl: viewModel.profileOfUser.imageBG,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 3 / 10,
                  fit: BoxFit.cover,
                )
              : Image.asset(
                  'assets/images/bgProfile.png',
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 3 / 10,
                  fit: BoxFit.cover,
                ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: WidgetAvatar(
            // avatar
            profile: viewModel.profileOfUser,
            onPress: () {
              _handleChooseImage(viewModel, 'avatar');
            },
          ),
        ),
        Align(
          alignment: Alignment.bottomRight,
          child: InkWell(
            // Button update imageBG
            onTap: () {
              _handleChooseImage(viewModel, 'coverImage');
            },
            child: Container(
              margin: EdgeInsets.only(bottom: 30, right: 10),
              width: 30,
              height: 30,
              decoration: BoxDecoration(
                  shape: BoxShape.circle, color: AppColors.greyColor),
              child: Image.asset(AppImages.iconCamera),
            ),
          ),
        ),
      ],
    );
  }

  _handleChooseImage(ProfileViewModel viewModel, String type) async {
    return showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) {
        return CupertinoActionSheet(
          actions: <Widget>[
            CupertinoActionSheetAction(
              child: Text('Take Photo'),
              onPressed: () {
                _checkPermissionCamera(viewModel, type);
                Navigator.pop(context);
                // _selectImage();
              },
            ),
            CupertinoActionSheetAction(
              child: Text('Choose From Gallery'),
              onPressed: () {
                _checkPermissionGallery(viewModel, type);
                Navigator.pop(context);
              },
            ),
          ],
          cancelButton: CupertinoActionSheetAction(
            isDefaultAction: true,
            child: Text('Cancel'),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        );
      },
    );
  }

  _checkPermissionCamera(ProfileViewModel viewModel, String type) async {
    var cameraStatus = await Permission.camera.status;
    print(cameraStatus);

    if (!cameraStatus.isGranted) {
      await Permission.camera.request();
    } else {
      _selectImage('camera', viewModel, type);
    }
  }

  _checkPermissionGallery(ProfileViewModel viewModel, String type) async {
    var galleryStatus = await Permission.storage.status;
    print(galleryStatus);

    if (!galleryStatus.isGranted) {
      await Permission.storage.request();
    } else if (galleryStatus.isGranted) {
      _selectImage('gallery', viewModel, type);
    }
  }

  _selectImage(
      String typePicker, ProfileViewModel viewModel, String type) async {
    dynamic pickedFile;
    if (typePicker == 'camera') {
      pickedFile = await picker.getImage(source: ImageSource.camera);
    } else {
      pickedFile = await picker.getImage(source: ImageSource.gallery);
    }

    if (pickedFile != null) {
      File _image = File(pickedFile.path);
      if (type == 'coverImage') {
        viewModel..updateCoverImage(_image);
      } else if (type == 'avatar') {
        viewModel..updateAva(_image);
      }
    } else {
      print('No image selected.');
    }
  }
}
