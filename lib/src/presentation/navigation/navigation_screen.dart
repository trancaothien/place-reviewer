import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';
import 'package:flutter_place_reviewer/src/presentation/login/login.dart';
import 'package:flutter_place_reviewer/src/presentation/presentation.dart';
import 'package:flutter_place_reviewer/src/utils/utils.dart';

class NavigationScreen extends StatefulWidget {
  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<NavigationViewModel>(
      viewModel: NavigationViewModel(),
      onViewModelReady: (viewModel) {
        viewModel.getProfile();
      },
      builder: (context, viewModel, child) {
        return _bodyBuilder(context, viewModel);
      },
    );
  }

  Widget _bodyBuilder(BuildContext context, NavigationViewModel viewModel) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.blueColor,
        title: viewModel.profileOfUser.name != null
            ? Text(
                'Hello, ${viewModel.profileOfUser.name}',
                style: TextStyle(color: Colors.white),
              )
            : Text(
                'Hello,',
                style: TextStyle(color: Colors.white),
              ),
        leading: Container(
          margin: EdgeInsets.only(left: 10, top: 5, bottom: 5),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(300.0)),
            child: viewModel.profileOfUser.avatar != null
                ? CachedNetworkImage(
                    imageUrl: viewModel.profileOfUser.avatar,
                    fit: BoxFit.cover,
                  )
                : Image.asset(
                    AppImages.defaultAvatar,
                    fit: BoxFit.cover,
                  ),
          ),
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            InkWell(
              child: Container(
                height: 50,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    border: Border(
                  bottom: BorderSide(width: 1.0, color: AppColors.greyColor),
                )),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Text(
                      'Chat',
                      style: AppStyles.DEFAULT_SMALL_BOLD,
                    ),
                  ),
                ),
              ),
              onTap: () {
                Navigator.pushNamed(context, Routers.friend);
              },
            ),
            Container(
              height: 50,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  border: Border(
                bottom: BorderSide(width: 1.0, color: AppColors.greyColor),
              )),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: InkWell(
                    onTap: () async {
                      await AppShared().setLogOut(true);
                      // Navigator.pushAndRemoveUntil(context, Routers.loginScreen, (route) => false);
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                              builder: (context) => LoginScreen()),
                          (Route<dynamic> route) => false);
                    },
                    child: Text(
                      'Log Out',
                      style: AppStyles.DEFAULT_SMALL_BOLD,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
