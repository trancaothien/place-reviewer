import 'package:flutter_place_reviewer/src/presentation/base/base_viewmodel.dart';
import 'package:flutter_place_reviewer/src/resource/database/firebase/firebase_realtime.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/models.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/profile.dart';
import 'package:flutter_place_reviewer/src/utils/utils.dart';

class NavigationViewModel extends BaseViewModel {
  Profile profileOfUser = Profile();
  FirebaseRealTime database = FirebaseRealTime.getInstance();

  getProfile() async {
    User user = await AppShared().getAccount();
    database.getProfile(user.email, (profile) {
      profileOfUser = profile;
      notifyListeners();
    });
  }
}
