import 'package:flutter_place_reviewer/src/presentation/base/base.dart';
import 'package:flutter_place_reviewer/src/resource/database/firebase/firebase_realtime.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/models.dart';

class GroupViewModel extends BaseViewModel {
  List<Post> posts = [];

  FirebaseRealTime database = FirebaseRealTime.getInstance();

  getPostWithCategoryAndCountry(String category, String country) {
    posts.clear();
    database.getPostWithCountryAndCategory(country, category, (post) {
      posts.add(post);
      notifyListeners();
    });
  }

  searchWithPlaceName(String value) {
    posts.clear();
    database.searchWithPlaceName(value, (post) {
      posts.add(post);
      notifyListeners();
    });
  }
}
