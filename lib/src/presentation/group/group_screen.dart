import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';
import 'package:flutter_place_reviewer/src/presentation/base/base.dart';
import 'package:flutter_place_reviewer/src/presentation/group/group.dart';
import 'package:flutter_place_reviewer/src/presentation/presentation.dart';
import 'package:flutter_place_reviewer/src/presentation/widgets/widget_card_post.dart';
import 'package:flutter_place_reviewer/src/presentation/widgets/widget_search_bar.dart';

class GroupScreen extends StatefulWidget {
  @override
  _GroupScreenState createState() => _GroupScreenState();
}

class _GroupScreenState extends State<GroupScreen> {
  List<String> categories;
  String _ratingCountryController;
  String _categoryChoose;
  TextEditingController searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    categories = _initTemp();

    return BaseWidget<GroupViewModel>(
      viewModel: GroupViewModel(),
      onViewModelReady: (viewModel) {
        viewModel.getPostWithCategoryAndCountry(null, null);
      },
      builder: (context, viewModel, child) {
        return _buildBody(context, viewModel);
      },
    );
  }

  Widget _buildBody(BuildContext context, GroupViewModel viewModel) {
    return Stack(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(AppImages.backgroundSplash3),
              fit: BoxFit.fill,
            ),
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            leading: Container(
              padding: EdgeInsets.only(left: 5),
              margin: EdgeInsets.only(top: 10, left: 10),
              child: _dropdownButton(viewModel),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(10),
                  ),
                  color: AppColors.blueColor),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: Center(
                  child: Text(
                    AppDefaults.categoryTitle,
                    style: TextStyle(
                        fontSize: AppStyles.FONT_SIZE_LARGE,
                        color: AppColors.blueColor),
                  ),
                ),
              )
            ],
            leadingWidth: 150.0,
          ),
          body: Container(
            child: _bodyGroup(context, viewModel),
          ),
        )
      ],
    );
  }

  Widget _bodyGroup(BuildContext context, GroupViewModel viewModel) {
    return Column(
      children: [
        Expanded(
          child: Container(
              margin: EdgeInsets.only(bottom: 10),
              child: WidgetSearchBar(
                hint: 'Search',
                onChange: (value) {
                  setState(() {
                    viewModel.searchWithPlaceName(value);
                  });
                },
                seachController: searchController,
              )),
          flex: 0,
        ),
        Expanded(
          child: _listTitle(viewModel),
          flex: 0,
        ),
        Expanded(
          child: Container(
            child: ListView.builder(
              itemCount: viewModel.posts.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: WidgetCardPost(
                    positionName: viewModel.posts[index].placeName,
                    country: viewModel.posts[index].countryName,
                    isBookmark: false,
                    image: viewModel.posts[index].uriOfImage,
                    onPress: () {
                      Navigator.pushNamed(context, Routers.detailPost,
                          arguments: viewModel.posts[index].postId);
                    },
                  ),
                );
              },
            ),
          ),
          flex: 8,
        ),
      ],
    );
  }

  Widget _dropdownButton(GroupViewModel viewModel) {
    return DropdownButtonFormField(
      dropdownColor: AppColors.blueColor,
      value: _ratingCountryController,
      style: TextStyle(color: Colors.white, fontFamily: AppStyles.FONT_MEDIUM),
      items: ['Canada', 'Việt Nam', 'Indonesia', 'Thailand', 'Korea']
          .map(
            (String label) => DropdownMenuItem(
              child: Text(label.toString()),
              value: label,
            ),
          )
          .toList(),
      onChanged: (value) {
        setState(() {
          _ratingCountryController = value;
          viewModel.getPostWithCategoryAndCountry(
              _categoryChoose, _ratingCountryController);
        });
      },
      hint: Center(
        child: Text(
          'Country',
          style: TextStyle(
              color: Colors.white, fontSize: AppStyles.FONT_SIZE_SMALL),
        ),
      ),
      decoration: InputDecoration(border: InputBorder.none),
    );
  }

  Widget _listTitle(GroupViewModel viewModel) {
    return Container(
      height: 30,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: categories.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              setState(() {
                _categoryChoose = categories[index];
                viewModel.getPostWithCategoryAndCountry(
                    _categoryChoose, _ratingCountryController);
              });
            },
            child: _itemsSearchTitle(categories[index]),
          );
        },
      ),
    );
  }

  Widget _itemsSearchTitle(String name) {
    return Container(
      height: 30,
      margin: EdgeInsets.only(left: 10),
      padding: EdgeInsets.all(5),
      constraints: BoxConstraints(
        minWidth: 70,
      ),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(25),
          ),
          border: Border.all(color: AppColors.greyColor, width: 1.0),
          color: name == _categoryChoose ? AppColors.blueColor : Colors.white),
      child: Center(
        child: Text(
          name,
          style: TextStyle(
              fontFamily: AppStyles.FONT_MEDIUM,
              color: name == _categoryChoose ? Colors.white : Colors.black),
        ),
      ),
    );
  }

  List<String> _initTemp() {
    List<String> countries = List<String>();
    countries.add('Hotel');
    countries.add('River');
    countries.add('Mountain');
    countries.add('Lake');
    countries.add('City');
    countries.add('Beach');
    return countries;
  }
}
