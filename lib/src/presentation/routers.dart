import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/presentation/account/account.dart';
import 'package:flutter_place_reviewer/src/presentation/bottom_navigation/bottom_navigation.dart';
import 'package:flutter_place_reviewer/src/presentation/chat/chat.dart';
import 'package:flutter_place_reviewer/src/presentation/forgot_pass/forgot_pass_screen.dart';
import 'package:flutter_place_reviewer/src/presentation/friend/friend_screen.dart';
import 'package:flutter_place_reviewer/src/presentation/friend_request/friend_request_screen.dart';
import 'package:flutter_place_reviewer/src/presentation/login/login_screen.dart';
import 'package:flutter_place_reviewer/src/presentation/show_post/show_post.dart';
import 'account/account_screen.dart';
import 'detail_post/detail_post.dart';

class Routers {
  static const String navigation = "/";
  static const String loginScreen = '/loginScreen';
  static const String detailPost = '/detailPost';
  static const String showAllPost = '/showPost';
  static const String bottomNavigation = '/bottomNavigation';
  static const String signUp = '/signUp';
  static const String friend = '/friend';
  static const String friendRequest = '/friendRequest';
  static const String chat = '/chat';
  static const String forgot_pass = '/forgot';

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case navigation:
        return MaterialPageRoute(
            builder: (context) => BottomNavigationScreen());
        break;
      case signUp:
        return MaterialPageRoute(builder: (context) => AccountScreen());
        break;
      case forgot_pass:
        return MaterialPageRoute(builder: (context) => ForgotPassScreen());
        break;
      case loginScreen:
        return MaterialPageRoute(builder: (context) => LoginScreen());
        break;
      case chat:
        return MaterialPageRoute(
            builder: (context) => ChatScreen(email: settings.arguments));
        break;
      case detailPost:
        return MaterialPageRoute(
            builder: (context) => DetailPostScreen(postId: settings.arguments));
        break;
      case showAllPost:
        return MaterialPageRoute(
            builder: (context) => ShowPostScreen(
                  title: settings.arguments,
                ));
        break;
      case bottomNavigation:
        return MaterialPageRoute(
            builder: (context) => BottomNavigationScreen());
        break;
      case bottomNavigation:
        return MaterialPageRoute(builder: (context) => AccountScreen());
        break;
      case friend:
        return MaterialPageRoute(builder: (context) => FriendScreen());
        break;
      case friendRequest:
        return MaterialPageRoute(builder: (context) => FriendRequestScreen());
        break;
      default:
        return null;
    }
  }
}
