import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';
import 'package:flutter_place_reviewer/src/presentation/presentation.dart';
import 'package:flutter_place_reviewer/src/presentation/show_post/show_post.dart';
import 'package:flutter_place_reviewer/src/presentation/widgets/widget_card_post.dart';

class ShowPostScreen extends StatefulWidget {
  String title;

  ShowPostScreen({this.title});

  @override
  _ShowPostScreenState createState() => _ShowPostScreenState(title: title);
}

class _ShowPostScreenState extends State<ShowPostScreen> {
  String title;

  _ShowPostScreenState({this.title});

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ShowPostViewModel>(
      viewModel: ShowPostViewModel(),
      onViewModelReady: (viewModel) {
        print(title);
        if (title == 'Show Post') {
          viewModel..getPostWithPageView();
        } else if (title == 'Popular Destination') {
          viewModel..getPopularDestination();
        }
      },
      builder: (context, viewModel, child) {
        return _bodyBuilder(context, viewModel);
      },
    );
  }

  Widget _bodyBuilder(BuildContext context, ShowPostViewModel viewModel) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(AppImages.backgroundSplash3),
              fit: BoxFit.fill,
            ),
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: _appBar(),
          body: _bodyShowAllPost(context, viewModel),
        ),
      ],
    );
  }

  Widget _appBar() {
    return AppBar(
      backgroundColor: Colors.transparent,
      leading: IconButton(
        icon: Image.asset(
          AppImages.iconBackArrow,
          scale: 2,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.only(right: 20),
          child: Center(
            child: Text(
              title,
              style: TextStyle(
                  fontSize: AppStyles.FONT_SIZE_LARGE,
                  color: AppColors.blueColor),
            ),
          ),
        )
      ],
      automaticallyImplyLeading: false,
    );
  }

  Widget _bodyShowAllPost(BuildContext context, ShowPostViewModel viewModel) {
    return Container(
      child: NotificationListener<ScrollEndNotification>(
        child: ListView.builder(
          itemCount: viewModel.posts.length,
          itemBuilder: (context, index) {
            if (index == viewModel.posts.length && viewModel.posts.isNotEmpty) {
              return Padding(
                padding: const EdgeInsets.all(30.0),
                child: Center(
                  child: viewModel.posts.length % 10 == 0
                      ? CircularProgressIndicator()
                      : null,
                ),
              );
            } else {
              return Container(
                padding: EdgeInsets.only(left: 10, right: 10),
                child: WidgetCardPost(
                  positionName: viewModel.posts[index].placeName,
                  country: viewModel.posts[index].countryName,
                  isBookmark: false,
                  image: viewModel.posts[index].uriOfImage,
                  onPress: () {
                    Navigator.pushNamed(context, Routers.detailPost,
                        arguments: viewModel.posts[index].postId);
                  },
                ),
              );
            }
          },
        ),
        onNotification: (scrollEnd) {
          var metrics = scrollEnd.metrics;
          if (metrics.atEdge) {
            if (metrics.pixels == 0)
              print('At top');
            else
              print('At bottom');
            setState(() {
              if (title == 'Show Post') {
                viewModel.getPostWithPageView();
              } else if (title == 'Popular Destination') {
                viewModel.getPopularDestination();
              }
            });
          }
          return true;
        },
      ),
    );
  }
}
