import 'package:flutter_place_reviewer/src/resource/database/firebase/firebase_realtime.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/models.dart';
import '../presentation.dart';

class ShowPostViewModel extends BaseViewModel {
  List<Post> posts = [];
  final int pageLimit = 10;
  int pageEndOffset = 0;
  String postId;
  FirebaseRealTime database = FirebaseRealTime.getInstance();

  getPostWithPageView() async {
    if (posts.isNotEmpty) {
      postId = posts[posts.length - 1].postId;
      print('postID: ' + postId);
    }
    if ((pageEndOffset) % 10 == 0) {
      await database.getPostWithPageView(postId, pageLimit, (Post values) {
        // print('list post ${values[0].postId}');
        // if (pageEndOffset == 0) {
        // posts.addAll(pageEndOffset, post);
        if (posts.length % 10 == 1 && posts.length >= 10) {
          posts.insert(posts.length - 1, values);
        } else {
          posts.insert(posts.length, values);
        }
        pageEndOffset++;
        notifyListeners();
        // } else {
        //   if (pageEndOffset >= 10) {
        //     if (pageEndOffset % 10 != 1) {
        //       posts.insert(pageEndOffset, post);
        //       pageEndOffset++;
        //       notifyListeners();
        //     }
        //   } else {
        //     posts.insert(pageEndOffset, post);
        //     pageEndOffset++;
        //     notifyListeners();
        //   }
        // }
      });
    }
    print(pageEndOffset);
  }

  getPopularDestination() {
    if (posts.isNotEmpty) {
      postId = posts[posts.length - 1].postId;
      print('postID: ' + postId);
    }
    if ((pageEndOffset) % 10 == 0) {
      database.getAllPopularDestination(postId, pageLimit, (post) {
        // if ((pageEndOffset) % 10 == 0) {
        //   posts.insert(pageEndOffset - 1, post);
        // } else {
        posts.insert(pageEndOffset, post);
        // }
        pageEndOffset++;
        notifyListeners();
      });
    }
    print(pageEndOffset);
  }
}
