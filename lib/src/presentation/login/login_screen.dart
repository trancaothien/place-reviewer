import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';
import 'package:flutter_place_reviewer/src/presentation/account/account_screen.dart';
import 'package:flutter_place_reviewer/src/presentation/base/base.dart';
import 'package:flutter_place_reviewer/src/presentation/bottom_navigation/bottom_navigation_screen.dart';
import 'package:flutter_place_reviewer/src/presentation/login/login.dart';
import 'package:flutter_place_reviewer/src/presentation/widgets/widget_button_orange.dart';
import 'package:flutter_place_reviewer/src/presentation/widgets/widget_input.dart';
import 'package:flutter_place_reviewer/src/presentation/widgets/widget_input_pass.dart';
import 'package:flutter_place_reviewer/src/utils/utils.dart';

import '../presentation.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<LoginViewModel>(
      viewModel: LoginViewModel(),
      onViewModelReady: (viewModel) {},
      builder: (context, viewModel, child) {
        return _buildBody(context, viewModel);
      },
    );
  }

  Widget _buildBody(BuildContext context, LoginViewModel viewModel) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: ExactAssetImage(AppImages.backgroundSplash3),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 20),
                child: Text(
                  'Login',
                  style: TextStyle(
                      color: AppColors.orangeColor,
                      fontSize: AppStyles.FONT_SIZE_LARGE,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Stack(
                children: [
                  Container(
                    margin: EdgeInsets.only(
                        bottom: 30, left: 20, right: 20, top: 20),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3),
                          )
                        ]),
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: _containerInput(viewModel),
                    ),
                  ),
                ],
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Don\'t have account? ',
                      style: TextStyle(color: Colors.white),
                    ),
                    InkWell(
                      onTap: () {
                        viewModel.goToSignUpScreen(context);
                      },
                      child: Text(
                        'Create a new account',
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _containerInput(LoginViewModel viewModel) {
    return Column(
      children: [
        WidgetInput(
          editingController: viewModel.editEmailController,
          label: 'Email',
          icon: AppImages.iconEmail,
        ),
        WidgetInputPass(
          controller: viewModel.editPassController,
          label: 'Password',
          icon: AppImages.iconPass,
          result: viewModel.signInResult,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20),
          child: Align(
              alignment: Alignment.topRight,
              child: InkWell(
                onTap: () {
                  viewModel.goToForgotPasswordScreen(context);
                },
                child: Text(
                  AppDefaults.forgotPass,
                  style: TextStyle(color: Colors.grey),
                ),
              )),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 30.0, bottom: 10.0),
          child: Align(
            alignment: Alignment.bottomCenter,
            child: WidgetButtonOrange(
              name: 'Login',
              width: 150.0,
              onPress: () async {
                EasyLoading.show(status: "Signing...");
                await viewModel.signIn();
                if (viewModel.signInResult == 'success') {
                  AppShared().setLogOut(false);
                  EasyLoading.dismiss();
                  EasyLoading.removeAllCallbacks();
                } else {
                  EasyLoading.dismiss();
                  EasyLoading.removeAllCallbacks();
                }
              },
            ),
          ),
        ),
      ],
    );
  }
}
