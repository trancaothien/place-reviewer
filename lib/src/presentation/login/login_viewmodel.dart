import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/resource/resource.dart';

import '../presentation.dart';

class LoginViewModel extends BaseViewModel {
  String signInResult = '';
  FirebaseAuthencation authencation = FirebaseAuthencation.getInstance();

  var editEmailController = TextEditingController();
  var editPassController = TextEditingController();

  signIn() async {
    await authencation
        .signIn(editEmailController.text, editPassController.text)
        .then((value) {
      signInResult = value;
      notifyListeners();
      if (value == 'success') {
        Navigator.popAndPushNamed(context, Routers.bottomNavigation);
      }
    });
  }

  goToForgotPasswordScreen(BuildContext context) {
    Navigator.pushNamed(context, Routers.forgot_pass);
  }

  goToSignUpScreen(BuildContext context) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => AccountScreen()));
  }
}
