import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';
import 'package:flutter_place_reviewer/src/presentation/widgets/widget_input.dart';
import '../presentation.dart';
import 'forgot_pass_viewmodel.dart';

class ForgotPassScreen extends StatefulWidget {
  @override
  _ForgotPassScreenState createState() => _ForgotPassScreenState();
}

class _ForgotPassScreenState extends State<ForgotPassScreen> {
  double appBarHeight = AppBar().preferredSize.height;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<ForgotPassViewModel>(
      viewModel: ForgotPassViewModel(),
      onViewModelReady: (viewModel) {},
      builder: (context, viewModel, child) {
        return _buildBody(viewModel, context);
      },
    );
  }

  Widget _buildBody(ForgotPassViewModel viewModel, BuildContext context) {
    return Scaffold(
      body: _bodyForgotPass(viewModel),
    );
  }

  Widget _bodyForgotPass(ForgotPassViewModel viewModel) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: ExactAssetImage(AppImages.backgroundSplash3),
                  fit: BoxFit.cover,
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 40),
                      child: Text(
                        'Forgot password',
                        style: TextStyle(
                            color: AppColors.orangeColor,
                            fontSize: AppStyles.FONT_SIZE_LARGE,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Stack(
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                            bottom: 30, left: 20, right: 20, top: 20),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset: Offset(0, 3),
                              )
                            ]),
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: _containerInput(viewModel),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.only(top: appBarHeight / 2),
                child: IconButton(
                  onPressed: () {
                    viewModel.backToLoginScreen(context);
                  },
                  icon: Icon(
                    Icons.arrow_back,
                    color: Colors.grey,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _containerInput(ForgotPassViewModel viewModel) {
    return Column(
      children: [
        WidgetInput(
          editingController: viewModel.editEmailController,
          label: 'Email',
          icon: AppImages.iconEmail,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: WidgetButtonOrange(
            name: 'Send',
            width: 150.0,
            onPress: () {
              viewModel.resetPasswod(context);
            },
          ),
        ),
      ],
    );
  }
}
