import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/resource/resource.dart';
import 'package:toast/toast.dart';

import '../presentation.dart';

class ForgotPassViewModel extends BaseViewModel {
  FirebaseAuthencation auth = FirebaseAuthencation.getInstance();
  String result = '';
  var editEmailController = TextEditingController();
  void resetPasswod(BuildContext context) {
    if (EmailValidator.validate(editEmailController.text)) {
      result = auth.resetPassword(editEmailController.text);
      if (result == 'success') {
        editEmailController.clear();
        Toast.show("Check your email", context,
            duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
      }
    } else {
      Toast.show("Email invalid", context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
  }

  backToLoginScreen(BuildContext context) {
    Navigator.pop(context);
  }
}
