import 'package:flutter_place_reviewer/src/resource/database/firebase/firebase_realtime.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/models.dart';
import 'package:rxdart/rxdart.dart';
import '../presentation.dart';

class BookmarkViewModel extends BaseViewModel {
  List<Post> posts = [];

  static var bookmarkStream = BehaviorSubject<String>();

  FirebaseRealTime database = FirebaseRealTime.getInstance();

  BookmarkViewModel() {
    updatePosts();
  }

  getAllBookmark() async {
    await database.getBookmark((post) {
      if (post != null) {
        posts.add(post);
        notifyListeners();
      }
    });
  }

  updatePosts() {
    bookmarkStream.listen((value) {
      print('stream: ' + value);
      if (value != null || value.length > 0) {
        for (int i = 0; i <= posts.length - 1; i++) {
          if (posts[i].postId == value) {
            posts.removeAt(i);
            notifyListeners();
            return;
          }
        }
      }
    });
  }
}
