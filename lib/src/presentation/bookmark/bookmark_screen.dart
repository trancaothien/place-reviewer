import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';
import 'package:flutter_place_reviewer/src/presentation/bookmark/bookmark.dart';
import 'package:flutter_place_reviewer/src/presentation/presentation.dart';
import 'package:flutter_place_reviewer/src/presentation/widgets/widget_card_post.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/post.dart';

class BookmarkScreen extends StatefulWidget {
  @override
  _BookmarkScreenState createState() => _BookmarkScreenState();
}

class _BookmarkScreenState extends State<BookmarkScreen> {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<BookmarkViewModel>(
      viewModel: BookmarkViewModel(),
      onViewModelReady: (viewModel) async {
        await viewModel.getAllBookmark();
      },
      builder: (context, viewModel, child) {
        return _buildBody(context, viewModel);
      },
    );
  }

  Widget _buildBody(BuildContext context, BookmarkViewModel viewModel) {
    return Stack(
      children: [
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(AppImages.backgroundSplash3),
              fit: BoxFit.fill,
            ),
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            automaticallyImplyLeading: false,
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: Center(
                  child: Text(
                    AppDefaults.bookMarkTitle,
                    style: TextStyle(
                        fontSize: AppStyles.FONT_SIZE_LARGE,
                        color: AppColors.blueColor),
                  ),
                ),
              )
            ],
          ),
          body: _bodyBookmark(context, viewModel),
        ),
      ],
    );
  }

  _bodyBookmark(BuildContext context, BookmarkViewModel viewModel) {
    return Container(
      child: viewModel.posts != null
          ? ListView.builder(
              itemCount: viewModel.posts.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: WidgetCardPost(
                    positionName: viewModel.posts[index].placeName,
                    country: viewModel.posts[index].countryName,
                    isBookmark: true,
                    image: viewModel.posts[index].uriOfImage,
                    onPress: () {
                      Navigator.pushNamed(context, Routers.detailPost,
                          arguments: viewModel.posts[index].postId);
                    },
                  ),
                );
              })
          : Container(),
    );
  }
}
