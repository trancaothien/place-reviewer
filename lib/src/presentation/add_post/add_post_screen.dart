import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';
import 'package:flutter_place_reviewer/src/presentation/add_post/add_post.dart';
import 'package:flutter_place_reviewer/src/presentation/presentation.dart';
import 'package:flutter_place_reviewer/src/presentation/widgets/widget_button_orange.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:toast/toast.dart';

class AddPostScreen extends StatefulWidget {
  @override
  _AddPostScreenState createState() => _AddPostScreenState();
}

class _AddPostScreenState extends State<AddPostScreen> {
  File _image;
  final picker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    return BaseWidget<AddPostViewModel>(
      viewModel: AddPostViewModel(),
      onViewModelReady: (viewModel) {},
      builder: (context, viewModel, child) {
        return _buildBody(context, viewModel);
      },
    );
  }

  Widget _buildBody(BuildContext context, AddPostViewModel viewModel) {
    return Stack(
      children: [
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(AppImages.backgroundSplash3),
              fit: BoxFit.fill,
            ),
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            automaticallyImplyLeading: false,
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: Center(
                  child: Text(
                    AppDefaults.addPostTitle,
                    style: TextStyle(
                        fontSize: AppStyles.FONT_SIZE_LARGE,
                        color: AppColors.blueColor),
                  ),
                ),
              )
            ],
          ),
          body: Container(
            height: MediaQuery.of(context).size.height,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    _dropdownCountry(viewModel),
                    Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: _dropdownCategory(viewModel),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: _chooseImage(context),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: _inputPlace(viewModel),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: _inputDesOfImage(viewModel),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15, bottom: 15),
                      child: WidgetButtonOrange(
                        name: 'Share',
                        width: MediaQuery.of(context).size.width,
                        onPress: () async {
                          EasyLoading.show(status: "Adding new post");
                          await viewModel.addNewPost(
                            _image,
                            (result) {
                              if (result) {
                                EasyLoading.dismiss();
                                EasyLoading.removeAllCallbacks();
                                Toast.show("successfully", context,
                                    duration: Toast.LENGTH_LONG,
                                    gravity: Toast.BOTTOM);
                                viewModel.placeEditing.clear();
                                viewModel.contentEditing.clear();
                                setState(() {
                                  _image = null;
                                  viewModel.ratingCategoryController = null;
                                  viewModel.ratingCountryController = null;
                                });
                              } else {
                                Toast.show("successfully", context,
                                    duration: Toast.LENGTH_LONG,
                                    gravity: Toast.BOTTOM);
                              }
                            },
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _dropdownCountry(AddPostViewModel viewModel) {
    return Container(
      height: 50,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          border: Border.all(width: 1.0, color: Colors.black),
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: DropdownButtonFormField(
          value: viewModel.ratingCountryController,
          items: ['Canada', 'Việt Nam', 'Indonesia', 'Thailand', 'Korea']
              .map((String label) => DropdownMenuItem(
                    child: Text(label.toString()),
                    value: label,
                  ))
              .toList(),
          hint: Center(
            child: Text(
              'Country',
            ),
          ),
          onChanged: (String value) {
            setState(() {
              viewModel.ratingCountryController = value;
            });
          },
          decoration: InputDecoration(border: InputBorder.none),
        ),
      ),
    );
  }

  Widget _dropdownCategory(AddPostViewModel viewModel) {
    return Container(
      height: 50,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        border: Border.all(width: 1.0, color: Colors.black),
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: DropdownButtonFormField(
          value: viewModel.ratingCategoryController,
          items: ['River', 'City', 'Mountain', 'Lake', 'Hotel', 'Beach']
              .map((String label) => DropdownMenuItem(
                    child: Text(label.toString()),
                    value: label,
                  ))
              .toList(),
          hint: Center(
            child: Text(
              'Category',
            ),
          ),
          onChanged: (String value) {
            setState(() {
              viewModel.ratingCategoryController = value;
            });
          },
          decoration: InputDecoration(border: InputBorder.none),
        ),
      ),
    );
  }

  Widget _chooseImage(BuildContext context) {
    return InkWell(
      onTap: () {
        _handleChooseImage();
      },
      child: Container(
        height: 200,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            border: Border.all(width: 1.0, color: Colors.black),
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: _image == null
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Choose Picture',
                    style: AppStyles.DEFAULT_MEDIUM,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Image.asset(AppImages.iconGallery),
                  )
                ],
              )
            : ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.file(
                  _image,
                  width: MediaQuery.of(context).size.width,
                  height: 200,
                  fit: BoxFit.cover,
                ),
              ),
      ),
    );
  }

  Widget _inputPlace(AddPostViewModel viewModel) {
    return Container(
      padding: EdgeInsets.only(right: 20, left: 20),
      width: MediaQuery.of(context).size.width,
      constraints: BoxConstraints(
        minHeight: 50.0,
      ),
      decoration: BoxDecoration(
          border: Border.all(width: 1, color: Colors.black),
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: TextFormField(
        maxLines: null,
        controller: viewModel.placeEditing,
        decoration:
            InputDecoration(border: InputBorder.none, hintText: 'Place'),
      ),
    );
  }

  Widget _inputDesOfImage(AddPostViewModel viewModel) {
    return Container(
      padding: EdgeInsets.only(right: 20, left: 20),
      width: MediaQuery.of(context).size.width,
      constraints: BoxConstraints(
        minHeight: 100.0,
      ),
      decoration: BoxDecoration(
          border: Border.all(width: 1, color: Colors.black),
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: TextFormField(
        controller: viewModel.contentEditing,
        maxLines: null,
        decoration:
            InputDecoration(border: InputBorder.none, hintText: 'About Photo'),
      ),
    );
  }

  _handleChooseImage() async {
    return showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) {
        return CupertinoActionSheet(
          actions: <Widget>[
            CupertinoActionSheetAction(
              child: Text('Take Photo'),
              onPressed: () {
                _checkPermissionCamera();
                Navigator.pop(context);
                // _selectImage();
              },
            ),
            CupertinoActionSheetAction(
              child: Text('Choose From Gallery'),
              onPressed: () {
                _checkPermissionGallery();
                Navigator.pop(context);
              },
            ),
          ],
          cancelButton: CupertinoActionSheetAction(
            isDefaultAction: true,
            child: Text('Cancel'),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        );
      },
    );
  }

  _checkPermissionCamera() async {
    var cameraStatus = await Permission.camera.status;
    print(cameraStatus);

    if (!cameraStatus.isGranted) {
      await Permission.camera.request();
    } else {
      _selectImage('camera');
    }
  }

  _checkPermissionGallery() async {
    var galleryStatus = await Permission.storage.status;
    print(galleryStatus);

    if (!galleryStatus.isGranted) {
      await Permission.storage.request();
    } else if (galleryStatus.isGranted) {
      _selectImage('gallery');
    }
  }

  _selectImage(String typePicker) async {
    dynamic pickedFile;
    if (typePicker == 'camera') {
      pickedFile = await picker.getImage(source: ImageSource.camera);
    } else {
      pickedFile = await picker.getImage(source: ImageSource.gallery);
    }

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }
}
