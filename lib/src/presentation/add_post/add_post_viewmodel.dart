import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/presentation/presentation.dart';
import 'package:flutter_place_reviewer/src/resource/database/firebase/firebase_realtime.dart';

class AddPostViewModel extends BaseViewModel {
  FirebaseRealTime database = FirebaseRealTime.getInstance();
  String ratingCategoryController;
  String ratingCountryController;
  var placeEditing = TextEditingController();
  var contentEditing = TextEditingController();

  addNewPost(File image, success(bool result)) async {
    int currentTime = DateTime.now().millisecondsSinceEpoch;
    await database.addNewPost(
        image,
        placeEditing.text,
        contentEditing.text,
        ratingCountryController,
        currentTime,
        ratingCategoryController, (result) {
      success(true);
    });
  }
}
