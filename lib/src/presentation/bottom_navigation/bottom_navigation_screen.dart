import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';
import 'package:flutter_place_reviewer/src/presentation/add_post/add_post.dart';
import 'package:flutter_place_reviewer/src/presentation/bookmark/bookmark.dart';
import 'package:flutter_place_reviewer/src/presentation/bottom_navigation/bottom_navigation.dart';
import 'package:flutter_place_reviewer/src/presentation/group/group.dart';
import 'package:flutter_place_reviewer/src/presentation/home/home_screen.dart';
import 'package:flutter_place_reviewer/src/presentation/profile/profile.dart';
import 'package:toast/toast.dart';

import '../presentation.dart';

class BottomNavigationScreen extends StatefulWidget {
  @override
  _BottomNavigationScreenState createState() => _BottomNavigationScreenState();
}

class _BottomNavigationScreenState extends State<BottomNavigationScreen> {
  PageController _myPage = PageController(initialPage: 0);
  int _selectedIndex = 0;
  // FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  DateTime currentBackPressTime;
  @override
  void initState() {
    super.initState();
    // flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    // var initializationSettingsAndroid =
    //     AndroidInitializationSettings('launch_background');
    // var initializationSettingsIOs = IOSInitializationSettings();
    // var initSetttings = InitializationSettings(
    //     android: initializationSettingsAndroid, iOS: initializationSettingsIOs);

    // flutterLocalNotificationsPlugin.initialize(initSetttings,
    //     onSelectNotification: onSelectNotification);
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<BottomNavigationViewModel>(
      viewModel: BottomNavigationViewModel(),
      onViewModelReady: (viewModel) async {
        // await viewModel.showNotification(flutterLocalNotificationsPlugin);
      },
      builder: (context, viewModel, child) {
        return _buildBody(context, viewModel);
      },
    );
  }

  Widget _buildBody(BuildContext context, BottomNavigationViewModel viewModel) {
    return Scaffold(
      bottomNavigationBar: _bottomAppBar(),
      floatingActionButton: _floatingBottom(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      body: WillPopScope(
        child: _bodyOfBottomAppBar(),
        onWillPop: () => Future.value(false),
      ),
    );
  }

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Toast.show("Tap back again to leave!", context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
      return Future.value(false);
    }
    return Future.value(true);
  }

  Widget _bodyOfBottomAppBar() {
    return Container(
      child: PageView(
        controller: _myPage,
        onPageChanged: (int) {
          setState(() {
            _selectedIndex = int;
          });
        },
        children: <Widget>[
          Center(
            child: HomeScreen(),
          ),
          Center(
            child: GroupScreen(),
          ),
          Center(
            child: AddPostScreen(),
          ),
          Center(
            child: BookmarkScreen(),
          ),
          Center(
            child: ProfileScreen(),
          )
        ],
        physics:
            NeverScrollableScrollPhysics(), // Comment this if you need to use Swipe.
      ),
    );
  }

  Widget _floatingBottom() {
    return FloatingActionButton(
      elevation: 0,
      onPressed: () {
        setState(() {
          _myPage.jumpToPage(2);
        });
      },
      child: Icon(Icons.add),
      backgroundColor:
          _selectedIndex == 2 ? AppColors.blueColor : AppColors.orangeColor,
    );
  }

  Widget _bottomAppBar() {
    return BottomAppBar(
      child: Container(
        height: 50,
        padding: EdgeInsets.only(left: 20, right: 20),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            InkWell(
              child: Container(
                child: _selectedIndex != 0
                    ? Image.asset(
                        AppImages.iconHome,
                        scale: 2,
                      )
                    : Container(
                        width: 35,
                        height: 35,
                        padding: EdgeInsets.all(5),
                        child: Image.asset(
                          AppImages.iconHome,
                          scale: 2,
                          color: Colors.white,
                        ),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: AppColors.blueColor),
                      ),
              ),
              onTap: () {
                setState(() {
                  _myPage.jumpToPage(0);
                });
              },
            ),
            InkWell(
              child: Container(
                margin: EdgeInsets.only(right: 10),
                child: _selectedIndex != 1
                    ? Image.asset(
                        AppImages.iconGroup,
                        scale: 2,
                      )
                    : Container(
                        width: 35,
                        height: 35,
                        padding: EdgeInsets.all(5),
                        child: Image.asset(
                          AppImages.iconGroup,
                          scale: 2,
                          color: Colors.white,
                        ),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: AppColors.blueColor),
                      ),
              ),
              onTap: () {
                setState(() {
                  _myPage.jumpToPage(1);
                });
              },
            ),
            InkWell(
              child: Container(
                margin: EdgeInsets.only(left: 10),
                child: _selectedIndex != 3
                    ? Image.asset(
                        AppImages.iconBookmark,
                        scale: 2,
                      )
                    : Container(
                        width: 35,
                        height: 35,
                        padding: EdgeInsets.all(5),
                        child: Image.asset(
                          AppImages.iconBookmark,
                          scale: 2,
                          color: Colors.white,
                        ),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: AppColors.blueColor),
                      ),
              ),
              onTap: () {
                setState(() {
                  _myPage.jumpToPage(3);
                });
              },
            ),
            InkWell(
              child: Container(
                child: _selectedIndex != 4
                    ? Image.asset(
                        AppImages.iconProfile,
                        scale: 2,
                      )
                    : Container(
                        width: 35,
                        height: 35,
                        padding: EdgeInsets.all(5),
                        child: Image.asset(
                          AppImages.iconProfile,
                          scale: 2,
                          color: Colors.white,
                        ),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: AppColors.blueColor),
                      ),
              ),
              onTap: () {
                setState(() {
                  _myPage.jumpToPage(4);
                });
              },
            ),
          ],
        ),
      ),
      shape: CircularNotchedRectangle(),
      color: AppColors.greyColor,
    );
  }

  Future onSelectNotification(String payload) async {
    await Navigator.pushNamed(context, Routers.chat, arguments: payload);
  }
}
