import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';
import '../presentation.dart';

class FriendRequestScreen extends StatefulWidget {
  @override
  _FriendRequestScreenState createState() => _FriendRequestScreenState();
}

class _FriendRequestScreenState extends State<FriendRequestScreen> {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<FriendRequestViewModel>(
      viewModel: FriendRequestViewModel(),
      onViewModelReady: (viewModel) async {
        await viewModel.getAllRequest();
        viewModel.init();
      },
      builder: (context, viewModel, child) {
        return _bodyBuilder(context, viewModel);
      },
    );
  }

  Widget _bodyBuilder(BuildContext context, FriendRequestViewModel viewModel) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(AppImages.backgroundSplash3),
              fit: BoxFit.fill,
            ),
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: _appBar(),
          body: _bodyFriendRequest(viewModel),
        ),
      ],
    );
  }

  Widget _bodyFriendRequest(FriendRequestViewModel viewModel) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
        itemCount: viewModel.profiles.length,
        itemBuilder: (context, index) {
          return Container(
            padding: EdgeInsets.only(left: 10, right: 10, bottom: 5),
            child: Center(
              child: WidgetItemRequest(
                profile: viewModel.profiles[index],
                onPress: () {
                  viewModel.acceptRequest(
                    viewModel.requests[index].receiver,
                    viewModel.requests[index].sender,
                    viewModel.requests[index].requestId,
                    (result) {
                      viewModel.requests.removeAt(index);
                      viewModel.profiles.removeAt(index);
                      FriendRequestViewModel.acceptRequestStream.add(index);
                    },
                  );
                },
                name: "Accept",
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _appBar() {
    return AppBar(
      backgroundColor: Colors.transparent,
      leading: IconButton(
        icon: Image.asset(
          AppImages.iconBackArrow,
          scale: 2,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.only(right: 20),
          child: Center(
            child: Text(
              'Friend request',
              style: TextStyle(
                  fontSize: AppStyles.FONT_SIZE_LARGE,
                  color: AppColors.blueColor),
            ),
          ),
        )
      ],
      automaticallyImplyLeading: false,
    );
  }
}
