import 'package:flutter_place_reviewer/src/resource/database/firebase/firebase_realtime.dart';
import 'package:flutter_place_reviewer/src/resource/resource.dart';
import 'package:flutter_place_reviewer/src/utils/utils.dart';
import 'package:rxdart/rxdart.dart';
import '../presentation.dart';

class FriendRequestViewModel extends BaseViewModel {
  List<Profile> profiles = [];
  List<FriendRequest> requests = [];
  FirebaseRealTime database = FirebaseRealTime.getInstance();
  static var acceptRequestStream = BehaviorSubject<int>();

  init() {
    acceptRequestStream.listen((value) {
      print("index: $value");
      if (value != null) {
        requests.removeAt(value);
        profiles.removeAt(value);
        notifyListeners();
      }
    });
  }

  getAllRequest() async {
    User user = await AppShared().getAccount();
    database.getAllRequestFriend(user.uid, (request) {
      requests.add(request);
      database.getProfileWithUid(request.sender, (profile) {
        profiles.add(profile);
        notifyListeners();
      });
    });
  }

  acceptRequest(
      String uid, String sender, String requestId, success(bool result)) async {
    database.acceptRequest(uid, sender, requestId, (result) {
      if (result) {
        success(result);
      }
    });
  }
}
