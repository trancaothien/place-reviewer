import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';
import 'package:flutter_place_reviewer/src/presentation/base/base.dart';
import 'package:flutter_place_reviewer/src/presentation/friend/friend_viewmodel.dart';
import 'package:flutter_place_reviewer/src/presentation/widgets/widget_button_orange.dart';
import 'package:flutter_place_reviewer/src/presentation/widgets/widget_item_request.dart';
import 'package:toast/toast.dart';

import '../presentation.dart';

class FriendScreen extends StatefulWidget {
  @override
  _FriendScreenState createState() => _FriendScreenState();
}

class _FriendScreenState extends State<FriendScreen> {
  TextEditingController _edtEmail = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BaseWidget<FriendViewModel>(
      viewModel: FriendViewModel(),
      onViewModelReady: (viewModel) async {
        await viewModel.getFriends();
      },
      builder: (context, viewModel, child) {
        return _bodyBuilder(context, viewModel);
      },
    );
  }

  Widget _bodyBuilder(BuildContext context, FriendViewModel viewModel) {
    return Stack(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(AppImages.backgroundSplash3),
              fit: BoxFit.fill,
            ),
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: _appBar(),
          body: _bodyFriendScreen(viewModel),
        )
      ],
    );
  }

  Widget _bodyFriendScreen(FriendViewModel viewModel) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: _searchBar('Email', viewModel),
              flex: 1,
            ),
            Container(
              margin: EdgeInsets.only(top: 10, bottom: 10),
              height: 1.0,
              color: Colors.white,
              width: MediaQuery.of(context).size.width,
            ),
            Expanded(
              child: Container(
                child: _listFriend(viewModel),
              ),
              flex: 13,
            )
          ],
        ),
      ),
    );
  }

  Widget _listFriend(FriendViewModel viewModel) {
    return ListView.builder(
        itemCount: viewModel.profiles.length,
        itemBuilder: (context, index) {
          return Container(
            margin: EdgeInsets.only(left: 10, right: 10, bottom: 5),
            child: WidgetItemRequest(
              onPress: () {
                Navigator.pushNamed(context, Routers.chat,
                    arguments: viewModel.profiles[index].email);
              },
              name: "Chat",
              profile: viewModel.profiles[index],
            ),
          );
        });
  }

  Widget _appBar() {
    return AppBar(
      backgroundColor: Colors.transparent,
      leading: IconButton(
        icon: Image.asset(
          AppImages.iconBackArrow,
          scale: 2,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.only(right: 20),
          child: Center(
            child: Text(
              'Friend',
              style: TextStyle(
                  fontSize: AppStyles.FONT_SIZE_LARGE,
                  color: AppColors.blueColor),
            ),
          ),
        )
      ],
      automaticallyImplyLeading: false,
    );
  }

  Widget _searchBar(String name, FriendViewModel viewModel) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10.0),
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
          color: AppColors.greyColor),
      child: Row(
        children: [
          Expanded(
            child: Center(
              child: TextFormField(
                controller: _edtEmail,
                decoration:
                    InputDecoration(border: InputBorder.none, hintText: name),
              ),
            ),
            flex: 8,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(4.0),
              child: WidgetButtonOrange(
                name: 'Add friend',
                onPress: () async {
                  await viewModel.addFriend(
                    _edtEmail.text,
                    (result) {
                      if (result == true) {
                        Toast.show("Success", context,
                            duration: Toast.LENGTH_SHORT,
                            gravity: Toast.BOTTOM);
                        _edtEmail.clear();
                      } else {
                        Toast.show("Please check your email ", context,
                            duration: Toast.LENGTH_SHORT,
                            gravity: Toast.BOTTOM);
                      }
                    },
                  );
                },
              ),
            ),
            flex: 5,
          )
        ],
      ),
    );
  }
}
