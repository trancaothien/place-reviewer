import 'package:flutter_place_reviewer/src/presentation/presentation.dart';
import 'package:flutter_place_reviewer/src/resource/database/firebase/firebase_realtime.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/friend_request.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/models.dart';

import 'package:flutter_place_reviewer/src/resource/database/models/profile.dart';
import 'package:flutter_place_reviewer/src/utils/utils.dart';

class FriendViewModel extends BaseViewModel {
  List<Profile> profiles = [];
  FirebaseRealTime database = FirebaseRealTime.getInstance();

  init() {}

  addFriend(String email, success(bool result)) async {
    User user = await AppShared().getAccount();

    int currentTime = DateTime.now().millisecondsSinceEpoch;
    database.getUid(email, (uid) {
      if (user.uid == uid) {
        success(false);
      } else {
        FriendRequest friendRequest = FriendRequest(
            sender: user.uid,
            receiver: uid,
            // 0: is default requestFriend, 1: is accepted
            status: 0,
            createTime: currentTime);
        database.checkHaveFriend(friendRequest.receiver, friendRequest.sender,
            (result) {
          if (result) {
            database.addRequest(friendRequest, (result) {
              success(result);
            });
          }
        });
      }
    });
  }

  //get all friend
  getFriends() async {
    User user = await AppShared().getAccount();
    database.getAllFriend(user.uid, (uid) {
      database.getProfileWithUid(uid, (profile) {
        profiles.add(profile);
        notifyListeners();
      });
    });
  }
}
