import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';

class WidgetButtonBack extends StatelessWidget {
  Function onPress;
  double width;
  String name;

  WidgetButtonBack({this.onPress, this.name, this.width});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Container(
        width: width,
        height: 50,
        child: Center(
          child: Text(
            name,
            style: TextStyle(fontSize: AppStyles.FONT_SIZE_MEDIUM),
          ),
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(30)),
            color: Colors.white),
      ),
    );
  }
}
