import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/profile.dart';

class WidgetAvatar extends StatelessWidget {
  Profile profile;
  Function onPress;
  WidgetAvatar({this.profile, this.onPress});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 100,
      padding: EdgeInsets.all(3.0),
      decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.white),
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(300.0),
            child: profile.avatar != null
                ? CachedNetworkImage(
                    placeholder: (context, url) => Container(
                      color: Colors.white,
                      child: Center(
                        child: CircularProgressIndicator(
                          strokeWidth: 3.0,
                        ),
                      ),
                    ),
                    imageUrl: profile.avatar,
                    width: 100,
                    height: 100,
                    fit: BoxFit.cover,
                  )
                : Image.asset(
                    AppImages.defaultAvatar,
                    width: 100,
                    height: 100,
                    fit: BoxFit.cover,
                  ),
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: InkWell(
              // button update avatar
              onTap: onPress,
              child: Container(
                margin: EdgeInsets.only(bottom: 3, right: 3),
                width: 25,
                height: 25,
                decoration: BoxDecoration(
                    shape: BoxShape.circle, color: AppColors.greyColor),
                child: Image.asset(
                  AppImages.iconCamera,
                  scale: 1.5,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
