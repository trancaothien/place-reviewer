import 'package:blur/blur.dart';
import 'package:flutter/material.dart';

class WidgetButtonBlur extends StatelessWidget {
  String name;
  Function onPress;
  WidgetButtonBlur({this.name, this.onPress});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Container(
        child: Stack(
          children: [
            Blur(
              blur: 3,
              borderRadius: BorderRadius.circular(15),
              blurColor: Colors.black12,
              child: Container(
                width: 125,
                height: 50,
              ),
            ),
            Container(
              width: 125,
              height: 50,
              child: Center(
                child: Text(
                  name,
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.white),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
