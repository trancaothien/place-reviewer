import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';

class WidgetInput extends StatelessWidget {
  TextEditingController editingController;
  String label;
  String icon;
  WidgetInput({this.editingController, this.label, this.icon});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Expanded(
            child: TextFormField(
              controller: editingController,
              decoration:
                  InputDecoration(border: InputBorder.none, labelText: label),
            ),
            flex: 9,
          ),
          Expanded(
            child: Image.asset(
              icon,
              scale: 1.5,
            ),
            flex: 1,
          )
        ],
      ),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(color: Colors.grey[300], width: 2.0),
        ),
      ),
    );
  }
}
