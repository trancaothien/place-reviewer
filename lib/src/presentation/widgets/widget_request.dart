import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';

class WidgetRequest extends StatelessWidget {
  String name;
  int count;
  Function onPress;
  WidgetRequest({this.name, this.count, this.onPress});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        width: MediaQuery.of(context).size.width / 2,
        decoration: BoxDecoration(
          border: Border(
            top: BorderSide(width: 1, color: AppColors.greyColor),
            bottom: BorderSide(width: 1, color: AppColors.greyColor),
            right: BorderSide(width: 0.5, color: AppColors.greyColor),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: Text('$count'),
            ),
            Center(
              child: Text(name),
            )
          ],
        ),
      ),
      onTap: onPress,
    );
  }
}
