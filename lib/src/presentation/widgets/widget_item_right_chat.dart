import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/chat_model.dart';

class WidgetItemRightChat extends StatelessWidget {
  ChatModel chat;

  WidgetItemRightChat({this.chat});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Align(
            alignment: Alignment.centerRight,
            child: Container(
              constraints: new BoxConstraints(
                minHeight: 50.0,
              ),
              padding: EdgeInsets.all(10.0),
              margin: EdgeInsets.only(right: 10, bottom: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    chat.message,
                    style: TextStyle(color: Colors.white),
                  ),
                  Text(
                    chat.status == 0 ? "Đã nhận" : "Đã xem",
                    style: TextStyle(
                      fontSize: 11,
                      fontStyle: FontStyle.italic,
                      color: Colors.black45,
                    ),
                  ),
                ],
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(20),
                ),
                color: Colors.black45,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
