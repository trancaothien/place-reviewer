import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/chat_model.dart';

class WidgetItemLeftChat extends StatelessWidget {
  ChatModel chat;

  WidgetItemLeftChat({this.chat});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              constraints: new BoxConstraints(
                minHeight: 50.0,
              ),
              padding: EdgeInsets.all(10.0),
              margin: EdgeInsets.only(left: 10, bottom: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(chat.message),
                ],
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  color: Colors.black12),
            ),
          ),
        ],
      ),
    );
  }
}
