import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';

class WidgetCardPost extends StatelessWidget {
  String image;
  String positionName;
  String country;
  bool isBookmark;
  Function onPress;
  WidgetCardPost({
    this.image,
    this.positionName,
    this.country,
    this.isBookmark,
    this.onPress,
  });
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        margin: EdgeInsets.only(top: 5, bottom: 5),
        width: MediaQuery.of(context).size.width,
        height: 150,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.black45,
              blurRadius: 5.0,
              offset: Offset(0.0, 0.75),
            )
          ],
        ),
        child: Stack(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(15.0),
              child: CachedNetworkImage(
                imageUrl: image,
                placeholder: (context, url) => Container(
                  color: Colors.white,
                  child: Center(
                    child: CircularProgressIndicator(
                      strokeWidth: 3.0,
                    ),
                  ),
                ),
                width: MediaQuery.of(context).size.width,
                height: 200,
                fit: BoxFit.cover,
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 65,
                child: Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15, top: 5),
                  child: Column(
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          positionName,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                            fontSize: AppStyles.FONT_SIZE_SMALL,
                            shadows: [
                              Shadow(
                                  // bottomLeft
                                  offset: Offset(-0.5, -0.5),
                                  color: Colors.grey),
                              Shadow(
                                  // bottomRight
                                  offset: Offset(0.5, -0.5),
                                  color: Colors.grey),
                              Shadow(
                                  // topRight
                                  offset: Offset(0.5, 0.5),
                                  color: Colors.grey),
                              Shadow(
                                  // topLeft
                                  offset: Offset(-0.5, 0.5),
                                  color: Colors.grey),
                            ],
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          country,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                            fontSize: AppStyles.FONT_SIZE_MEDIUM,
                            shadows: [
                              Shadow(
                                  // bottomLeft
                                  offset: Offset(-0.5, -0.5),
                                  color: Colors.grey),
                              Shadow(
                                  // bottomRight
                                  offset: Offset(0.5, -0.5),
                                  color: Colors.grey),
                              Shadow(
                                  // topRight
                                  offset: Offset(0.5, 0.5),
                                  color: Colors.grey),
                              Shadow(
                                  // topLeft
                                  offset: Offset(-0.5, 0.5),
                                  color: Colors.grey),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            isBookmark
                ? Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      width: 45,
                      height: 45,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(15),
                        ),
                        color: AppColors.orangeColor,
                      ),
                      child: Center(
                        child: Image.asset(
                          AppImages.iconHaveBookmark,
                          scale: 1.5,
                        ),
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
      ),
      onTap: onPress,
    );
  }
}
