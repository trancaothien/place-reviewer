import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';

class WidgetCardHotel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 100,
      margin: EdgeInsets.only(top: 15, left: 20, right: 20),
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(20)),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black45,
            blurRadius: 5.0,
            offset: Offset(0.0, 0.75),
          ),
        ],
      ),
      child: Row(
        children: [
          Expanded(
            child: Image.asset(
              'assets/images/BALI.png',
              height: 100,
              fit: BoxFit.cover,
            ),
            flex: 2,
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Ramada Encore',
                      style: AppStyles.DEFAULT_SMALL_BOLD,
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 5),
                      child: Text(
                        'Etiam facilisis ligula nec velit posuere egestas.',
                        style: AppStyles.DEFAULT_SMALL,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            flex: 8,
          ),
        ],
      ),
    );
  }
}
