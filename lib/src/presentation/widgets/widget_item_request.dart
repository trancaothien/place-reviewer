import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/profile.dart';

class WidgetItemRequest extends StatelessWidget {
  Function onPress;
  Profile profile;
  String name;

  WidgetItemRequest({this.onPress, this.profile, this.name});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 60.0,
      padding: EdgeInsets.only(left: 5, right: 10),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: Stack(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Row(
              children: [
                ClipRRect(
                  child: profile.avatar == null
                      ? Image.asset(
                          AppImages.defaultAvatar,
                          width: 50,
                          height: 50,
                        )
                      : CachedNetworkImage(
                          imageUrl: profile.avatar,
                          width: 50,
                          height: 50,
                          fit: BoxFit.fill,
                        ),
                  borderRadius: BorderRadius.all(Radius.circular(300)),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: Text(
                    profile.name,
                    style: AppStyles.DEFAULT_SMALL_BOLD,
                  ),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: InkWell(
              onTap: onPress,
              child: Container(
                width: 90,
                height: 40,
                decoration: BoxDecoration(
                    color: AppColors.orangeColor,
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: Center(
                  child: Text(name, style: AppStyles.DEFAULT_SMALL_BOLD,),
                ),
              ),
            )
          )
        ],
      ),
    );
  }
}
