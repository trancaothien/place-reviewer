import 'package:flutter/material.dart';

class WidgetInputPass extends StatelessWidget {
  String icon;
  TextEditingController controller;
  String result = '';
  String label;
  WidgetInputPass({this.icon, this.controller, this.result, this.label});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Expanded(
            child: TextFormField(
              obscureText: true,
              controller: controller,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  labelText: label,
                  errorText: result != '' ? result : null),
            ),
            flex: 9,
          ),
          Expanded(
            child: Image.asset(
              icon,
              scale: 1.5,
            ),
            flex: 1,
          )
        ],
      ),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(color: Colors.grey[300], width: 2.0),
        ),
      ),
    );
  }
}
