import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/confligs.dart';
import 'package:flutter_place_reviewer/src/resource/resource.dart';

class WidgetItemPopular extends StatelessWidget {
  Post post;
  Function onPress;
  WidgetItemPopular({this.post, this.onPress});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        padding: EdgeInsets.only(bottom: 10),
        width: MediaQuery.of(context).size.width * 0.7,
        child: Stack(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(15.0),
              child: CachedNetworkImage(
                imageUrl: post.uriOfImage,
                placeholder: (context, url) => Container(
                  color: Colors.white,
                  child: Center(
                    child: CircularProgressIndicator(
                      strokeWidth: 3.0,
                    ),
                  ),
                ),
                width: MediaQuery.of(context).size.width,
                height: 400,
                fit: BoxFit.cover,
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 10, left: 10),
              height: 400,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Text(
                      post.placeName,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: AppStyles.FONT_SIZE_MEDIUM,
                        shadows: [
                          Shadow(
                              // bottomLeft
                              offset: Offset(-0.5, -0.5),
                              color: Colors.grey),
                          Shadow(
                              // bottomRight
                              offset: Offset(0.5, -0.5),
                              color: Colors.grey),
                          Shadow(
                              // topRight
                              offset: Offset(0.5, 0.5),
                              color: Colors.grey),
                          Shadow(
                              // topLeft
                              offset: Offset(-0.5, 0.5),
                              color: Colors.grey),
                        ],
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Text(
                      post.countryName,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: AppStyles.FONT_SIZE_LARGE,
                        shadows: [
                          Shadow(
                              // bottomLeft
                              offset: Offset(-0.5, -0.5),
                              color: Colors.grey),
                          Shadow(
                              // bottomRight
                              offset: Offset(0.5, -0.5),
                              color: Colors.grey),
                          Shadow(
                              // topRight
                              offset: Offset(0.5, 0.5),
                              color: Colors.grey),
                          Shadow(
                              // topLeft
                              offset: Offset(-0.5, 0.5),
                              color: Colors.grey),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      onTap: onPress,
    );
  }
}
