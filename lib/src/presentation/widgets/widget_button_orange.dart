import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';

class WidgetButtonOrange extends StatelessWidget {
  double width;
  String name;
  Function onPress;

  WidgetButtonOrange({this.width, this.name, this.onPress});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Container(
        width: width,
        height: 50,
        child: Center(
          child: Text(
            name,
            style: TextStyle(
                color: Colors.white, fontSize: AppStyles.FONT_SIZE_MEDIUM),
          ),
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(30)),
            color: AppColors.orangeColor),
      ),
    );
  }
}
