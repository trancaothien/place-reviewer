import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';

class WidgetSearchBar extends StatelessWidget {
  Function onChange;
  TextEditingController seachController;
  String hint;

  WidgetSearchBar({this.onChange, this.seachController, this.hint});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 10, right: 10, top: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
          color: AppColors.greyColor),
      child: Row(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Image.asset(
                AppImages.iconSearch,
                scale: 1.5,
              ),
            ),
            flex: 1,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: TextFormField(
                  decoration:
                      InputDecoration(border: InputBorder.none, hintText: hint),
                  controller: seachController,
                  onChanged: onChange),
            ),
            flex: 9,
          )
        ],
      ),
    );
  }
}
