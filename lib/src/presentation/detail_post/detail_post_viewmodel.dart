import 'package:flutter_place_reviewer/src/presentation/presentation.dart';
import 'package:flutter_place_reviewer/src/resource/database/firebase/firebase_realtime.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/models.dart';

class DetailPostViewModel extends BaseViewModel {
  Post post = Post();
  bool addBookmarkResult = false;
  FirebaseRealTime database = FirebaseRealTime.getInstance();

  getPost(String postId) {
    database.getDetailPost(postId, (value) {
      post = value;
      updateView(postId);
      notifyListeners();
    });
  }

  updateView(String postId) async {
    int views = post.view + 1;
    await database.updateViewOfPost(postId, views);
  }

  addBookmark(String postId) async {
    await database.addBookmark(postId, (bool) {
      addBookmarkResult = bool;
    });
    print(addBookmarkResult);
    notifyListeners();
  }

  checkBookmark(String postID) async {
    await database.checkBookmark(postID, (isHaveBookmark) {
      addBookmarkResult = isHaveBookmark;
      notifyListeners();
    });
  }

  removeBookmark(String postID) async {
    await database.removeBookmark(postID, () {
      BookmarkViewModel.bookmarkStream.add(postID);
      addBookmarkResult = false;
      notifyListeners();
    });
  }
}
