import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';
import 'package:flutter_place_reviewer/src/presentation/detail_post/detail_post.dart';
import 'package:flutter_place_reviewer/src/presentation/presentation.dart';
import 'package:flutter_place_reviewer/src/presentation/widgets/widget_card_hotel.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/models.dart';

class DetailPostScreen extends StatelessWidget {
  String postId;

  DetailPostScreen({this.postId});

  @override
  Widget build(BuildContext context) {
    return BaseWidget<DetailPostViewModel>(
      viewModel: DetailPostViewModel(),
      onViewModelReady: (viewModel) async {
        viewModel.getPost(postId);
        await viewModel.checkBookmark(postId);
      },
      builder: (context, viewModel, child) {
        return _buildBody(context, viewModel);
      },
    );
  }

  Widget _buildBody(BuildContext context, DetailPostViewModel viewModel) {
    return Scaffold(
      body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 3 / 10,
                  child: _headerPost(context, viewModel),
                ),
                Container(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 20, left: 20),
                        child: _contentPost(context, viewModel),
                      ),
                      // Container(
                      //   height: 40.0,
                      //   padding: EdgeInsets.only(left: 20, right: 20),
                      //   child: Stack(
                      //     children: [
                      //       Align(
                      //         alignment: Alignment.centerLeft,
                      //         child: Text(
                      //           'Hotel in ${viewModel.post.placeName}',
                      //           style: AppStyles.DEFAULT_MEDIUM,
                      //         ),
                      //       ),
                      //       Align(
                      //         alignment: Alignment.centerRight,
                      //         child: Text(
                      //           'View All',
                      //           style: TextStyle(
                      //               color: Colors.grey,
                      //               fontSize: AppStyles.FONT_SIZE_SMALL),
                      //         ),
                      //       ),
                      //     ],
                      //   ),
                      // ),
                      // Container(
                      //   width: MediaQuery.of(context).size.width,
                      //   margin: EdgeInsets.only(bottom: 20),
                      //   child: Column(
                      //     children: [
                      //       WidgetCardHotel(),
                      //       WidgetCardHotel(),
                      //       WidgetCardHotel(),
                      //       WidgetCardHotel(),
                      //     ],
                      //   ),
                      // )
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }

  //headerPost
  Widget _headerPost(BuildContext context, DetailPostViewModel viewModel) {
    return Stack(
      children: [
        CachedNetworkImage(
          placeholder: (context, url) => Container(
            color: Colors.white,
            child: Center(
              child: CircularProgressIndicator(
                strokeWidth: 3.0,
              ),
            ),
          ),
          imageUrl: viewModel.post.uriOfImage,
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height * 4 / 10,
          fit: BoxFit.cover,
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: 20,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(20),
                topLeft: Radius.circular(20),
              ),
              color: Colors.white,
            ),
          ),
        ),
        Align(
          alignment: Alignment.topLeft,
          child: Padding(
            padding: const EdgeInsets.only(top: 40),
            child: IconButton(
              icon: Image.asset(
                AppImages.iconBackArrow,
                color: Colors.white,
                scale: 2,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
        ),
      ],
    );
  }

  //contentPost
  Widget _contentPost(BuildContext context, DetailPostViewModel viewModel) {
    return Column(
      children: [
        Stack(
          children: [
            Align(
                alignment: Alignment.centerLeft,
                child: Row(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 2 / 3,
                      child: Text(
                        '${viewModel.post.placeName}, ${viewModel.post.countryName}',
                        style: AppStyles.DEFAULT_MEDIUM,
                      ),
                    )
                  ],
                )),
            Align(
              alignment: Alignment.centerRight,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text('${viewModel.post.view}'),
                  Padding(
                    padding: const EdgeInsets.only(left: 5, right: 0),
                    child: Image.asset(
                      AppImages.iconViews,
                      scale: 3,
                    ),
                  ),
                  IconButton(
                    icon: viewModel.addBookmarkResult
                        ? Image.asset(
                            AppImages.iconHaveBookmark,
                            color: AppColors.orangeColor,
                            scale: 1.5,
                          )
                        : Image.asset(
                            AppImages.iconAddBookmark,
                            color: Colors.black,
                            scale: 1.5,
                          ),
                    onPressed: () async {
                      if (viewModel.addBookmarkResult) {
                        await viewModel.removeBookmark(postId);
                      } else
                        await viewModel.addBookmark(postId);
                    },
                  )
                ],
              ),
            )
          ],
        ),
        Container(
            margin: EdgeInsets.only(bottom: 5, top: 5),
            child: Text('${viewModel.post.contentOfPost}'))
      ],
    );
  }
}
