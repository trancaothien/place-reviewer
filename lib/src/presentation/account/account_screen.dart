import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';
import 'package:flutter_place_reviewer/src/presentation/account/account.dart';
import 'package:flutter_place_reviewer/src/presentation/base/base.dart';
import 'package:flutter_place_reviewer/src/presentation/widgets/widget_input.dart';
import 'package:flutter_place_reviewer/src/presentation/widgets/widget_input_pass.dart';

import '../presentation.dart';

class AccountScreen extends StatefulWidget {
  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  double appBarHeight = AppBar().preferredSize.height;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<AccountViewModel>(
      viewModel: AccountViewModel(),
      onViewModelReady: (viewModel) {},
      builder: (context, viewModel, child) {
        return _buildBody(context, viewModel);
      },
    );
  }

  Widget _buildBody(BuildContext context, AccountViewModel viewModel) {
    return Scaffold(
      body: _bodyCreateAccount(context, viewModel),
    );
  }

  _bodyCreateAccount(BuildContext context, AccountViewModel viewModel) {
    return SingleChildScrollView(
      child: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: ExactAssetImage(AppImages.backgroundSplash3),
                fit: BoxFit.cover,
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 40),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 40),
                      child: Text(
                        'Create Account',
                        style: TextStyle(
                            color: AppColors.orangeColor,
                            fontSize: AppStyles.FONT_SIZE_LARGE,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Stack(
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                            bottom: 30, left: 20, right: 20, top: 20),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset: Offset(0, 3),
                              )
                            ]),
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: _input(context, viewModel),
                        ),
                      ),
                    ],
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Already have a account? ',
                            style: TextStyle(color: Colors.white)),
                        InkWell(
                          onTap: () {
                            viewModel.backToLoginScreen(context);
                          },
                          child: Text('Login'),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.only(top: appBarHeight / 2),
              child: IconButton(
                onPressed: () {
                  viewModel.backToLoginScreen(context);
                },
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.grey,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _input(BuildContext context, AccountViewModel viewModel) {
    return Column(
      children: [
        WidgetInput(
          editingController: viewModel.editNameController,
          label: 'Name',
          icon: AppImages.iconUser,
        ),
        WidgetInput(
          editingController: viewModel.editEmailController,
          label: 'Email',
          icon: AppImages.iconEmail,
        ),
        WidgetInputPass(
          controller: viewModel.editPassController,
          label: 'Password',
          icon: AppImages.iconPass,
          result: null,
        ),
        WidgetInputPass(
          controller: viewModel.editConfirmPassController,
          label: 'Confirm password',
          icon: AppImages.iconPass,
          result: null,
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: const EdgeInsets.only(top: 20.0, bottom: 10.0),
            child: WidgetButtonOrange(
              name: 'Sign Up',
              width: 150.0,
              onPress: () async {
                EasyLoading.show(status: "Creating...");
                await viewModel.signUp();
              },
            ),
          ),
        ),
      ],
    );
  }
}
