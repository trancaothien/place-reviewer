import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_place_reviewer/src/resource/database/firebase/firebase_authencation.dart';

import '../presentation.dart';

class AccountViewModel extends BaseViewModel {
  String signUpResult = '';
  FirebaseAuthencation firebaseAuthencation =
      FirebaseAuthencation.getInstance();

  var editNameController = TextEditingController();
  var editEmailController = TextEditingController();
  var editPassController = TextEditingController();
  var editConfirmPassController = TextEditingController();

  signUp() async {
    if (editPassController.text != editConfirmPassController.text) {
      signUpResult = 'not-confirm-pass';
      EasyLoading.dismiss();
      EasyLoading.removeAllCallbacks();
      notifyListeners();
    } else {
      await firebaseAuthencation
          .signUp(editPassController.text, editEmailController.text,
              editNameController.text)
          .then((value) {
        signUpResult = value;
        notifyListeners();
        if (signUpResult == 'success') {
          Navigator.pop(context);
          EasyLoading.dismiss();
          EasyLoading.removeAllCallbacks();
        } else if (signUpResult != '' && signUpResult != 'success') {
          EasyLoading.dismiss();
          EasyLoading.removeAllCallbacks();
        }
      });
    }
  }

  backToLoginScreen(BuildContext context) {
    Navigator.pop(context);
  }
}
