import 'package:flutter_place_reviewer/src/configs/constants/constants.dart';
import 'package:flutter_place_reviewer/src/resource/database/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppShared {

  saveAccount(String email, String uid, String pass) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString(AppDefaults.keyUserName, email);
    await preferences.setString(AppDefaults.keyUid, uid);
    await preferences.setString(AppDefaults.keyPass, pass);
  }

  Future<User> getAccount() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String email = preferences.getString(AppDefaults.keyUserName);
    String uid = preferences.getString(AppDefaults.keyUid);
    String pass = preferences.getString(AppDefaults.keyPass);
    return User(email: email, uid: uid, pass: pass);
  }

  setLogOut(bool isLogOut) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setBool(AppDefaults.keyLogOut, isLogOut);
  }

  Future<bool> isLogOut() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getBool(AppDefaults.keyLogOut);
  }
}
